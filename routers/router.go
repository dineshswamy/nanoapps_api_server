// @APIVersion 1.0.0
// @Title beego Test API
// @Description beego has a very cool tools to autogenerate documents for your API
// @Contact astaxie@gmail.com
// @TermsOfServiceUrl http://beego.me/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"nanoapps_api_server/controllers"

	"github.com/astaxie/beego"
)

func init() {
	ns := beego.NewNamespace("/api",
		beego.NSNamespace("/developer",
			beego.NSInclude(
				&controllers.DeveloperController{},
			),
		),
		beego.NSNamespace("/project",
			beego.NSInclude(
				&controllers.ProjectController{},
			),
		),
		beego.NSNamespace("/nanoapp",
			beego.NSInclude(
				&controllers.NanoappController{},
			),
		),
		beego.NSNamespace("/user",
			beego.NSInclude(
				&controllers.UserController{},
			),
		),
		beego.NSNamespace("/device",
			beego.NSInclude(
				&controllers.DeviceController{},
			),
		),
		beego.NSNamespace("/campaign",
			beego.NSInclude(
				&controllers.CampaignController{},
			),
		),
		beego.NSNamespace("/abtest",
			beego.NSInclude(
				&controllers.AbtestController{},
			),
		),
	)
	beego.AddNamespace(ns)
}
