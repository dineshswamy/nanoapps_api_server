package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:AbtestController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:AbtestController"],
        beego.ControllerComments{
            Method: "AddAbTestAnalytics",
            Router: `/add_analytics`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:AbtestController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:AbtestController"],
        beego.ControllerComments{
            Method: "AddVariation",
            Router: `/add_variation`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:AbtestController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:AbtestController"],
        beego.ControllerComments{
            Method: "Create",
            Router: `/create`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:CampaignController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:CampaignController"],
        beego.ControllerComments{
            Method: "Create",
            Router: `/create`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:CampaignController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:CampaignController"],
        beego.ControllerComments{
            Method: "Get",
            Router: `/details/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:CampaignController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:CampaignController"],
        beego.ControllerComments{
            Method: "StartCampaign",
            Router: `/start`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeveloperController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeveloperController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeveloperController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeveloperController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeveloperController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeveloperController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeveloperController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeveloperController"],
        beego.ControllerComments{
            Method: "Login",
            Router: `/login`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeveloperController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeveloperController"],
        beego.ControllerComments{
            Method: "Register",
            Router: `/register`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeviceController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeviceController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeviceController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeviceController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeviceController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeviceController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeviceController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeviceController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeviceController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeviceController"],
        beego.ControllerComments{
            Method: "AddDevice",
            Router: `/add`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeviceController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:DeviceController"],
        beego.ControllerComments{
            Method: "AddPushToken",
            Router: `/add_push_token`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"],
        beego.ControllerComments{
            Method: "GetNanoappInstalls",
            Router: `/:nanoapp_id/install_rate`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"],
        beego.ControllerComments{
            Method: "GetNanoappDurationDetails",
            Router: `/:nanoapp_id/time_duration`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"],
        beego.ControllerComments{
            Method: "AddNanoappInstall",
            Router: `/add_install`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"],
        beego.ControllerComments{
            Method: "AddUsageDetails",
            Router: `/add_usage_details`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"],
        beego.ControllerComments{
            Method: "GetNanoappDetails",
            Router: `/get_nanoapp_details`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"],
        beego.ControllerComments{
            Method: "UpdateNanoapp",
            Router: `/update`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:NanoappController"],
        beego.ControllerComments{
            Method: "UploadNanoapp",
            Router: `/upload`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:ProjectController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:ProjectController"],
        beego.ControllerComments{
            Method: "Create",
            Router: `/create`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:ProjectController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:ProjectController"],
        beego.ControllerComments{
            Method: "GetNanoappsOfProject",
            Router: `/get_nanoapps`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:ProjectController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:ProjectController"],
        beego.ControllerComments{
            Method: "GetProjectsOfDev",
            Router: `/get_projects_of_dev`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:ProjectController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:ProjectController"],
        beego.ControllerComments{
            Method: "UpdateProject",
            Router: `/update`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:ProjectController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:ProjectController"],
        beego.ControllerComments{
            Method: "VerifyProjectToken",
            Router: `/verify_token`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:UserController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:UserController"],
        beego.ControllerComments{
            Method: "GetAll",
            Router: `/`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:UserController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:UserController"],
        beego.ControllerComments{
            Method: "GetOne",
            Router: `/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:UserController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:UserController"],
        beego.ControllerComments{
            Method: "Put",
            Router: `/:id`,
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:UserController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:UserController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: `/:id`,
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:UserController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:UserController"],
        beego.ControllerComments{
            Method: "Create",
            Router: `/create`,
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["nanoapps_api_server/controllers:UserController"] = append(beego.GlobalControllerRouter["nanoapps_api_server/controllers:UserController"],
        beego.ControllerComments{
            Method: "PrintHello",
            Router: `/hello/:id`,
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
