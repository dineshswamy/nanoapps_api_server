package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CreateUserDevicesTable_20170905_063047 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreateUserDevicesTable_20170905_063047{}
	m.Created = "20170905_063047"

	migration.Register("CreateUserDevicesTable_20170905_063047", m)
}

// Run the migrations
func (m *CreateUserDevicesTable_20170905_063047) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`CREATE TABLE users_devices_data (
		id int(11) NOT NULL AUTO_INCREMENT,
		user_id INT NOT NULL,
		device_data_id INT NOT NULL,
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		deleted_at datetime DEFAULT NULL,		
		PRIMARY KEY (id),
		FOREIGN KEY (user_id) REFERENCES users(id),
		FOREIGN KEY (device_data_id) REFERENCES devices_data(id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8`)
}

// Reverse the migrations
func (m *CreateUserDevicesTable_20170905_063047) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL("DROP TABLE users_devices_data")
}
