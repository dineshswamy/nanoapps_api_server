package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type AddProjectIdToDevicesData_20171005_010124 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &AddProjectIdToDevicesData_20171005_010124{}
	m.Created = "20171005_010124"

	migration.Register("AddProjectIdToDevicesData_20171005_010124", m)
}

// Run the migrations
func (m *AddProjectIdToDevicesData_20171005_010124) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL("ALTER TABLE devices_data ADD project_id INT NOT NULL AFTER device_type")
	m.SQL("ALTER TABLE devices_data ADD CONSTRAINT fk_project_id FOREIGN KEY (project_id) REFERENCES projects(id);")
}

// Reverse the migrations
func (m *AddProjectIdToDevicesData_20171005_010124) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL("ALTER TABLE devices_data DROP FOREIGN KEY fk_project_id")
	m.SQL("ALTER TABLE devices_data DROP COLUMN project_id")
}
