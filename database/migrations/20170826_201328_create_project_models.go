package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CreateProjectModels_20170826_201328 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreateProjectModels_20170826_201328{}
	m.Created = "20170826_201328"

	migration.Register("CreateProjectModels_20170826_201328", m)
}

// Run the migrations
func (m *CreateProjectModels_20170826_201328) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`CREATE TABLE projects (
		id int(11) NOT NULL AUTO_INCREMENT,
		name varchar(255) NOT NULL,
		os varchar(120) NOT NULL,
		store_identifier varchar(255) NOT NULL,
		token varchar(255) NOT NULL,
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		deleted_at datetime DEFAULT NULL,
		PRIMARY KEY (id),
		UNIQUE KEY store_identifier (store_identifier)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8`)
}

// Reverse the migrations
func (m *CreateProjectModels_20170826_201328) Down() {
	// use
	m.SQL("DROP TABLE projects")
}
