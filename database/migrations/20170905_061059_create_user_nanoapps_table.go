package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CreateUserNanoappsTable_20170905_061059 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreateUserNanoappsTable_20170905_061059{}
	m.Created = "20170905_061059"

	migration.Register("CreateUserNanoappsTable_20170905_061059", m)
}

// Run the migrations
func (m *CreateUserNanoappsTable_20170905_061059) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`CREATE TABLE user_nanoapps (
		id int(11) NOT NULL AUTO_INCREMENT,
		user_id INT DEFAULT NULL,
		nanoapp_id INT NOT NULL,
		status VARCHAR(120) NOT NULL,	
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		deleted_at datetime DEFAULT NULL,		
		PRIMARY KEY (id),
		FOREIGN KEY (nanoapp_id) REFERENCES nanoapps(id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8`)
}

// Reverse the migrations
func (m *CreateUserNanoappsTable_20170905_061059) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL("DROP TABLE user_nanoapps")
}
