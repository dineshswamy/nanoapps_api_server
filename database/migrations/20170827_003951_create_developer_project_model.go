package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CreateDeveloperProjectModel_20170827_003951 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreateDeveloperProjectModel_20170827_003951{}
	m.Created = "20170827_003951"

	migration.Register("CreateDeveloperProjectModel_20170827_003951", m)
}

// Run the migrations
func (m *CreateDeveloperProjectModel_20170827_003951) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`CREATE TABLE developer_projects (
		id int(11) NOT NULL AUTO_INCREMENT,
		developer_id INT NOT NULL,
		project_id INT NOT NULL,
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		deleted_at datetime DEFAULT NULL,		
		PRIMARY KEY (id),
		FOREIGN KEY (developer_id) REFERENCES developers(id),
		FOREIGN KEY (project_id) REFERENCES projects(id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8`)
}

// Reverse the migrations
func (m *CreateDeveloperProjectModel_20170827_003951) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL("DROP TABLE developer_projects")
}
