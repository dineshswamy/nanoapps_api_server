package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type AddPublishedFlagToNanoapps_20170920_104601 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &AddPublishedFlagToNanoapps_20170920_104601{}
	m.Created = "20170920_104601"

	migration.Register("AddPublishedFlagToNanoapps_20170920_104601", m)
}

// Run the migrations
func (m *AddPublishedFlagToNanoapps_20170920_104601) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`ALTER TABLE nanoapps ADD COLUMN published TINYINT NOT NULL DEFAULT 0 AFTER bundle_url`)
}

// Reverse the migrations
func (m *AddPublishedFlagToNanoapps_20170920_104601) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL(`ALTER TABLE nanoapps DROP COLUMN published`)
}
