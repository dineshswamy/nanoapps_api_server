package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CreateAbTestsTable_20171106_123201 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreateAbTestsTable_20171106_123201{}
	m.Created = "20171106_123201"

	migration.Register("CreateAbTestsTable_20171106_123201", m)
}

// Run the migrations
func (m *CreateAbTestsTable_20171106_123201) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`CREATE TABLE ab_tests (
		id int(11) NOT NULL AUTO_INCREMENT,
		name varchar(255) NOT NULL,
		project_id INT NOT NULL,
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		deleted_at datetime DEFAULT NULL,		
		PRIMARY KEY (id),
		FOREIGN KEY (project_id) REFERENCES projects(id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8`)

}

// Reverse the migrations
func (m *CreateAbTestsTable_20171106_123201) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL("DROP TABLE ab_tests")
}
