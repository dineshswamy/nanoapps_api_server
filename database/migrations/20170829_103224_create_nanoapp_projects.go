package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CreateNanoappProjects_20170829_103224 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreateNanoappProjects_20170829_103224{}
	m.Created = "20170829_103224"

	migration.Register("CreateNanoappProjects_20170829_103224", m)
}

// Run the migrations
func (m *CreateNanoappProjects_20170829_103224) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`CREATE TABLE nanoapp_projects (
		id int(11) NOT NULL AUTO_INCREMENT,
		nanoapp_id INT NOT NULL,
		project_id INT NOT NULL,
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		deleted_at datetime DEFAULT NULL,		
		PRIMARY KEY (id),
		FOREIGN KEY (nanoapp_id) REFERENCES nanoapps(id),
		FOREIGN KEY (project_id) REFERENCES projects(id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8`)

}

// Reverse the migrations
func (m *CreateNanoappProjects_20170829_103224) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL("DROP TABLE nanoapp_projects")
}
