package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type MakeDeviceIdAndManufacturerIdUnique_20171004_230203 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &MakeDeviceIdAndManufacturerIdUnique_20171004_230203{}
	m.Created = "20171004_230203"

	migration.Register("MakeDeviceIdAndManufacturerIdUnique_20171004_230203", m)
}

// Run the migrations
func (m *MakeDeviceIdAndManufacturerIdUnique_20171004_230203) Up() {
	m.SQL("ALTER TABLE devices_data ADD CONSTRAINT unique_manufacturer_id UNIQUE (device_manufacturer_id);")
}

// Reverse the migrations
func (m *MakeDeviceIdAndManufacturerIdUnique_20171004_230203) Down() {
	m.SQL("DROP INDEX unique_manufacturer_id ON devices_data")
}
