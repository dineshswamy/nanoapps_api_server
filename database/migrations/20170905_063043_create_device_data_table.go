package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CreateDeviceDataTable_20170905_063043 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreateDeviceDataTable_20170905_063043{}
	m.Created = "20170905_063043"

	migration.Register("CreateDeviceDataTable_20170905_063043", m)
}

// Run the migrations
func (m *CreateDeviceDataTable_20170905_063043) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`CREATE TABLE devices_data (
		id int(11) NOT NULL AUTO_INCREMENT,
		os VARCHAR(120) NOT NULL,
		timezone VARCHAR(120) NOT NULL,
		language VARCHAR(120) NOT NULL,
		sdk VARCHAR(120) NOT NULL,
		sdk_type VARCHAR(120) NOT NULL,
		ad_id VARCHAR(255) NOT NULL,
		device_manufacturer_id VARCHAR(255) NOT NULL,
		device_model VARCHAR(120) NOT NULL,
		device_type VARCHAR(120) NOT NULL,
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		deleted_at datetime DEFAULT NULL,		
		PRIMARY KEY (id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8`)
}

// Reverse the migrations
func (m *CreateDeviceDataTable_20170905_063043) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL("DROP TABLE devices_data")
}
