package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CreateAbTestsVariationsTable_20171106_123202 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreateAbTestsVariationsTable_20171106_123202{}
	m.Created = "20171106_123202"

	migration.Register("CreateAbTestsVariationsTable_20171106_123202", m)
}

// Run the migrations
func (m *CreateAbTestsVariationsTable_20171106_123202) Up() {
	m.SQL(`CREATE TABLE ab_tests_variations (
		id int(11) NOT NULL AUTO_INCREMENT,
		name VARCHAR(120) NOT NULL,
		test_index INT DEFAULT NULL,
		ab_test_id INT DEFAULT NULL,
		nanoapp_id INT NOT NULL,
		status VARCHAR(120) NOT NULL,
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		deleted_at datetime DEFAULT NULL,		
		PRIMARY KEY (id),
		FOREIGN KEY (ab_test_id) REFERENCES ab_tests(id),
		FOREIGN KEY (nanoapp_id) REFERENCES nanoapps(id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8`)
}

// Reverse the migrations
func (m *CreateAbTestsVariationsTable_20171106_123202) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL("DROP TABLE ab_test_variations")

}
