package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type AddVersionCodeAndVersionName_20170908_185801 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &AddVersionCodeAndVersionName_20170908_185801{}
	m.Created = "20170908_185801"

	migration.Register("AddVersionCodeAndVersionName_20170908_185801", m)
}

// Run the migrations
func (m *AddVersionCodeAndVersionName_20170908_185801) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`ALTER TABLE nanoapps ADD COLUMN 
		version_code INT NOT NULL AFTER bundle_url,
		ADD COLUMN version_name VARCHAR(12) NOT NULL AFTER version_code`)

}

// Reverse the migrations
func (m *AddVersionCodeAndVersionName_20170908_185801) Down() {
	m.SQL(`ALTER TABLE nanoapps DROP COLUMN "version_code",
		   DROP COLUMN "version_name"`)
}
