package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CreateAbTestsAnalytics_20171106_123203 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreateAbTestsAnalytics_20171106_123203{}
	m.Created = "20171106_123203"

	migration.Register("CreateAbTestsAnalytics_20171106_123203", m)
}

// Run the migrations
func (m *CreateAbTestsAnalytics_20171106_123203) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`CREATE TABLE ab_tests_analytics (
		id int(11) NOT NULL AUTO_INCREMENT,
		ab_test_variation_id INT NOT NULL,
		goal_value INT NOT NULL DEFAULT 0,
		start_time datetime NOT NULL,
		end_time datetime NOT NULL,
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		deleted_at datetime DEFAULT NULL,
		PRIMARY KEY (id),
		FOREIGN KEY (ab_test_variation_id) REFERENCES ab_tests_variations(id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8`)
}

// Reverse the migrations
func (m *CreateAbTestsAnalytics_20171106_123203) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL("DROP TABLE ab_tests_analytics")
}
