package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type AddMainComponentNameField_20171002_170909 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &AddMainComponentNameField_20171002_170909{}
	m.Created = "20171002_170909"

	migration.Register("AddMainComponentNameField_20171002_170909", m)
}

// Run the migrations
func (m *AddMainComponentNameField_20171002_170909) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`ALTER TABLE nanoapps ADD COLUMN main_component_name VARCHAR(255) NOT NULL DEFAULT "MainComponent" AFTER bundle_url`)
}

// Reverse the migrations
func (m *AddMainComponentNameField_20171002_170909) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL(`ALTER TABLE nanoapps DROP COLUMN main_component_name`)
}
