package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CreateUsersTable_20170905_061055 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreateUsersTable_20170905_061055{}
	m.Created = "20170905_061055"

	migration.Register("CreateUsersTable_20170905_061055", m)
}

// Run the migrations
func (m *CreateUsersTable_20170905_061055) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`CREATE TABLE users (
		  id int(11) NOT NULL AUTO_INCREMENT,
		  unique_user_identifier varchar(255) NOT NULL,
		  project_id int(11) NOT NULL,
		  push_token varchar(255) DEFAULT NULL,
		  auth_token varchar(255) DEFAULT NULL,
		  created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  deleted_at datetime DEFAULT NULL,
		  PRIMARY KEY (id),
		  FOREIGN KEY (project_id) REFERENCES projects(id),
		  UNIQUE KEY composite_projectid_and_unique_user_identifier (unique_user_identifier, project_id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8`)
}

// Reverse the migrations
func (m *CreateUsersTable_20170905_061055) Down() {
	m.SQL("DROP TABLE users")
}
