package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type AddGoogleProjectIdAndGoogleServerApiKeyToProject_20171025_231844 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &AddGoogleProjectIdAndGoogleServerApiKeyToProject_20171025_231844{}
	m.Created = "20171025_231844"

	migration.Register("AddGoogleProjectIdAndGoogleServerApiKeyToProject_20171025_231844", m)
}

// Run the migrations
func (m *AddGoogleProjectIdAndGoogleServerApiKeyToProject_20171025_231844) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`ALTER TABLE projects ADD COLUMN google_sender_id VARCHAR(255) DEFAULT NULL AFTER token`)
	m.SQL(`ALTER TABLE projects ADD COLUMN google_server_api_key TEXT DEFAULT NULL AFTER google_sender_id`)
}

// Reverse the migrations
func (m *AddGoogleProjectIdAndGoogleServerApiKeyToProject_20171025_231844) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL(`ALTER TABLE projects DROP COLUMN google_sender_id`)
	m.SQL(`ALTER TABLE projects DROP COLUMN google_server_api_key`)
}
