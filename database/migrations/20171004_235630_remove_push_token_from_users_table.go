package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type RemovePushTokenFromUsersTable_20171004_235630 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &RemovePushTokenFromUsersTable_20171004_235630{}
	m.Created = "20171004_235630"

	migration.Register("RemovePushTokenFromUsersTable_20171004_235630", m)
}

// Run the migrations
func (m *RemovePushTokenFromUsersTable_20171004_235630) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL("ALTER TABLE users DROP COLUMN push_token;")
}

// Reverse the migrations
func (m *RemovePushTokenFromUsersTable_20171004_235630) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL("ALTER TABLE users ADD COLUMN push_token VARCHAR(255) NOT NULL;")
}
