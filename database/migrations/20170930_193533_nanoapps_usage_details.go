package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type NanoappsUsageDetails_20170930_193533 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &NanoappsUsageDetails_20170930_193533{}
	m.Created = "20170930_193533"

	migration.Register("NanoappsUsageDetails_20170930_193533", m)
}

// Run the migrations
func (m *NanoappsUsageDetails_20170930_193533) Up() {
	m.SQL(`CREATE TABLE nanoapp_usage_details (
		id int(11) NOT NULL AUTO_INCREMENT,
		nanoapp_id INT NOT NULL,
		start_time datetime NOT NULL,
		end_time datetime NOT NULL,
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		deleted_at datetime DEFAULT NULL,		
		PRIMARY KEY (id),
		FOREIGN KEY (nanoapp_id) REFERENCES nanoapps(id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8`)
}

// Reverse the migrations
func (m *NanoappsUsageDetails_20170930_193533) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL("DROP TABLE nanoapp_usage_details")
}
