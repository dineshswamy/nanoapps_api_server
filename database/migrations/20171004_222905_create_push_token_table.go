package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CreatePushTokenTable_20171004_222905 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreatePushTokenTable_20171004_222905{}
	m.Created = "20171004_222905"

	migration.Register("CreatePushTokenTable_20171004_222905", m)
}

// Run the migrations
func (m *CreatePushTokenTable_20171004_222905) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`CREATE TABLE push_tokens (
		id int(11) NOT NULL AUTO_INCREMENT,
		device_data_id INT NOT NULL,
		token VARCHAR(255) NOT NULL,
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		deleted_at datetime DEFAULT NULL,		
		PRIMARY KEY (id),
		FOREIGN KEY (device_data_id) REFERENCES devices_data(id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8`)
}

// Reverse the migrations
func (m *CreatePushTokenTable_20171004_222905) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL("DROP TABLE push_tokens")
}
