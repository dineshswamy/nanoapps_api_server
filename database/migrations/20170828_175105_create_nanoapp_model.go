package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CreateNanoappModel_20170828_175105 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreateNanoappModel_20170828_175105{}
	m.Created = "20170828_175105"

	migration.Register("CreateNanoappModel_20170828_175105", m)
}

// Run the migrations
func (m *CreateNanoappModel_20170828_175105) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`CREATE TABLE nanoapps (
		id int(11) NOT NULL AUTO_INCREMENT,
		name varchar(255) NOT NULL,
		description varchar(255) NOT NULL,
		image_url varchar(255) NOT NULL,
		bundle_url varchar(255) NOT NULL,
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		deleted_at datetime DEFAULT NULL,
		PRIMARY KEY (id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8`)
}

// Reverse the migrations
func (m *CreateNanoappModel_20170828_175105) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL("DROP TABLE nanoapps")
}
