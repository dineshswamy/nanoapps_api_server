package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type AddPackageNameToNanoapp_20170909_044755 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &AddPackageNameToNanoapp_20170909_044755{}
	m.Created = "20170909_044755"

	migration.Register("AddPackageNameToNanoapp_20170909_044755", m)
}

// Run the migrations
func (m *AddPackageNameToNanoapp_20170909_044755) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`ALTER TABLE nanoapps ADD COLUMN package_name 
		VARCHAR(120) NOT NULL AFTER bundle_url`)

}

// Reverse the migrations
func (m *AddPackageNameToNanoapp_20170909_044755) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL(`ALTER TABLE nanoapps DROP COLUMN "package_name"`)

}
