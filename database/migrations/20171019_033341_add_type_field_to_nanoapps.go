package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type AddTypeFieldToNanoapps_20171019_033341 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &AddTypeFieldToNanoapps_20171019_033341{}
	m.Created = "20171019_033341"

	migration.Register("AddTypeFieldToNanoapps_20171019_033341", m)
}

// Run the migrations
func (m *AddTypeFieldToNanoapps_20171019_033341) Up() {
	m.SQL(`ALTER TABLE nanoapps ADD COLUMN nanoapp_type VARCHAR(120) NOT NULL DEFAULT "ADDON" AFTER bundle_url`)
}

// Reverse the migrations
func (m *AddTypeFieldToNanoapps_20171019_033341) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL(`ALTER TABLE nanoapps DROP COLUMN nanoapp_type`)
}
