package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CreateCampaignsTable_20171005_174345 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreateCampaignsTable_20171005_174345{}
	m.Created = "20171005_174345"

	migration.Register("CreateCampaignsTable_20171005_174345", m)
}

// Run the migrations
func (m *CreateCampaignsTable_20171005_174345) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`CREATE TABLE campaigns (
		id int(11) NOT NULL AUTO_INCREMENT,
		name varchar(255) NOT NULL,
		title varchar(255) NOT NULL,
		subtitle varchar(255) NOT NULL,
		image_url varchar(255) NOT NULL,
		nanoapp_id INT NOT NULL, 
		created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
		updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		deleted_at datetime DEFAULT NULL,
		PRIMARY KEY (id),
		FOREIGN KEY (nanoapp_id) REFERENCES nanoapps(id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8`)
}

// Reverse the migrations
func (m *CreateCampaignsTable_20171005_174345) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL("DROP TABLE campaigns")
}
