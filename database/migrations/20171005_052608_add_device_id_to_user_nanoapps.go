package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type AddDeviceIdToUserNanoapps_20171005_052608 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &AddDeviceIdToUserNanoapps_20171005_052608{}
	m.Created = "20171005_052608"

	migration.Register("AddDeviceIdToUserNanoapps_20171005_052608", m)
}

// Run the migrations
func (m *AddDeviceIdToUserNanoapps_20171005_052608) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`ALTER TABLE user_nanoapps MODIFY user_id INT DEFAULT NULL;`)
	m.SQL(`ALTER TABLE user_nanoapps ADD COLUMN device_data_id INT DEFAULT NULL AFTER nanoapp_id`)
	m.SQL("ALTER TABLE user_nanoapps ADD CONSTRAINT fk_device_data_id FOREIGN KEY (device_data_id) REFERENCES devices_data(id);")
}

// Reverse the migrations
func (m *AddDeviceIdToUserNanoapps_20171005_052608) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL(`ALTER TABLE user_nanoapps MODIFY user_id INT DEFAULT NOT NULL;`)
	m.SQL(`ALTER TABLE user_nanoapps DROP FOREIGN KEY fk_device_data_id;`)
	m.SQL(`ALTER TABLE user_nanoapps DROP COLUMN device_data_id`)
}
