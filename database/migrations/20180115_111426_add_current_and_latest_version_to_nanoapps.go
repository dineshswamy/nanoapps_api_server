package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type AddCurrentAndLatestVersionToNanoapps_20180115_111426 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &AddCurrentAndLatestVersionToNanoapps_20180115_111426{}
	m.Created = "20180115_111426"

	migration.Register("AddCurrentAndLatestVersionToNanoapps_20180115_111426", m)
}

// Run the migrations
func (m *AddCurrentAndLatestVersionToNanoapps_20180115_111426) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`ALTER TABLE nanoapps 
			ADD COLUMN latest_version_code INT NOT NULL AFTER package_name,
			ADD COLUMN latest_version_name VARCHAR(100) NOT NULL AFTER latest_version_code,
			ADD COLUMN latest_checksum VARCHAR(200) NULL AFTER latest_version_name,
			CHANGE COLUMN version_code current_version_code INT NULL AFTER latest_checksum,
			CHANGE COLUMN version_name current_version_name VARCHAR(100) NULL AFTER current_version_code,
			ADD COLUMN current_checksum VARCHAR(200) NULL AFTER current_version_name
			`)
}

// Reverse the migrations
func (m *AddCurrentAndLatestVersionToNanoapps_20180115_111426) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update
	m.SQL(`ALTER TABLE nanoapps 
		   DROP COLUMN latest_version_code,
		   DROP COLUMN latest_version_name,
		   DROP COLUMN current_version_name,
		   DROP COLUMN current_version_code,
		   DROP COLUMN latest_checksum,
		   DROP COLUMN current_checksum,
		   CHANGE COLUMN current_version_code version_code INT NOT NULL
		   CHANGE COLUMN current_version_name version_name VARCHAR(100) NOT NULL
		   `)
}
