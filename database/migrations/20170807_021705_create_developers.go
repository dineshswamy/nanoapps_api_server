package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type CreateDevelopers_20170807_021705 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &CreateDevelopers_20170807_021705{}
	m.Created = "20170807_021705"

	migration.Register("CreateDevelopers_20170807_021705", m)
}

func (m *CreateDevelopers_20170807_021705) Up() {
	// use m.SQL("CREATE TABLE ...") to make schema update
	m.SQL(`CREATE TABLE developers (
  		  id int(11) NOT NULL AUTO_INCREMENT,
        	name varchar(255) NOT NULL,
  			phone varchar(255) DEFAULT NULL,
  			email varchar(255) NOT NULL,
  			password varchar(255) NOT NULL,
  			salt varchar(255) NOT NULL,
  			account_activated tinyint(1) NOT NULL DEFAULT '0',
  			last_login datetime NOT NULL,
  			created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  			updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  			deleted_at datetime DEFAULT NULL,
  			PRIMARY KEY (id),
  			UNIQUE KEY password (password),
  			UNIQUE KEY salt (salt)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8`)
}

// Reverse the migrations
func (m *CreateDevelopers_20170807_021705) Down() {
	m.SQL("DROP TABLE developers")
}
