package models

import (
	"fmt"
	"time"

	"github.com/astaxie/beego/orm"
)

type UserNanoapp struct {
	Id         int64       `orm:"auto;pk"`
	User       *User       `orm:"default(null);rel(fk)"`
	DeviceData *DeviceData `orm:"rel(fk)"`
	Nanoapp    *Nanoapp    `orm:"rel(fk)"`
	Status     string      `json:"status"`
	CreatedAt  time.Time   `orm:"auto_now_add;type(datetime)" json:"created_at"`
	UpdatedAt  time.Time   `orm:"auto_now;type(datetime)" json:"updated_at"`
	DeletedAt  time.Time   `orm:"auto_now_add;type(datetime)" json:"deleted_at"`
}

// TableName for user nanoapp ...
func (p *UserNanoapp) TableName() string {
	return "user_nanoapps"
}

func init() {
	orm.RegisterModel(new(UserNanoapp))
}

// AddUserNanoapp insert a new UserNanoapp into database and returns
// last inserted Id on success.
func AddUserNanoapp(m *UserNanoapp) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	time.Now()
	return
}

// GetUserNanoappById retrieves UserNanoapp by Id. Returns error if
// Id doesn't exist
func GetInstallRateForNanoapp(id int64) (v []UserNanoapp, err error) {
	o := orm.NewOrm()
	_, err = o.QueryTable(new(UserNanoapp)).Filter("nanoapp_id", id).OrderBy("created_at").All(&v)
	if err == nil {
		return v, nil
	}
	return nil, err
}

// UpdateUserNanoapp updates UserNanoapp by Id and returns error if
// the record to be updated doesn't exist
func UpdateUserNanoappById(m *UserNanoapp) (err error) {
	o := orm.NewOrm()
	v := UserNanoapp{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteUserNanoapp deletes UserNanoapp by Id and returns error if
// the record to be deleted doesn't exist
func DeleteUserNanoapp(id int64) (err error) {
	o := orm.NewOrm()
	v := UserNanoapp{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&UserNanoapp{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
