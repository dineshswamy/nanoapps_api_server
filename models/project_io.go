package models

type ProjectDetail struct {
	Name            string `json:"name"`
	Os              string `json:"os"`
	StoreIdentifier string `json:"store_identifier"`
	DeveloperId     int64  `json:"developer_id"`
}

type ProjectResponse struct {
	Status  string   `json:"status"`
	Code    int      `json:"code"`
	Message string   `json:"message"`
	Project *Project `json:"project"`
}

type ProjectTokenBody struct {
	Token string `json:"project_token"`
}

type ProjectNanoappsResponse struct {
	Status   string    `json:"status"`
	Code     int       `json:"code"`
	Message  string    `json:"message"`
	Nanoapps []Nanoapp `json:"nanoapps"`
}
