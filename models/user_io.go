package models

// UserRegistrationInfo
type UserCreateInfo struct {
	UniqueUserId      string `json:"unique_user_id"`
	ProjectIdentifier string `json:"project_token"`
}

type UserCreateInfoResponse struct {
	Status    string `json:"status"`
	Code      int    `json:"code"`
	Message   string `json:"message"`
	UserToken string `json:"user_auth_token"`
}
