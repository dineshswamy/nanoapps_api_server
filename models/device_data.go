package models

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type DeviceData struct {
	Id                   int64     `orm:"auto;pk"`
	Os                   string    `json:"os"`
	Timezone             string    `json:"timezone"`
	Language             string    `json:"language"`
	Sdk                  string    `json:"sdk"`
	SdkType              string    `json:"sdk_type"`
	AdId                 string    `json:"ad_id"`
	DeviceManufacturerId string    `json:"device_manufacturer_id"`
	DeviceModel          string    `json:"device_model"`
	DeviceType           string    `json:"device_type"`
	ProjectId            int64     `json:"project_id"`
	CreatedAt            time.Time `orm:"auto_now_add;type(datetime)" json:"created_at"`
	UpdatedAt            time.Time `orm:"auto_now;type(datetime)" json:"updated_at"`
	DeletedAt            time.Time `orm:"auto_now_add;type(datetime)" json:"deleted_at"`
}

// TableName for user ...
func (p *DeviceData) TableName() string {
	return "devices_data"
}

func init() {
	orm.RegisterModel(new(DeviceData))
}

// AddDeviceData insert a new DeviceData into database and returns
// last inserted Id on success.
func AddDeviceData(m *DeviceData) (read bool, id int64, err error) {
	o := orm.NewOrm()
	read, id, err = o.ReadOrCreate(m, "DeviceManufacturerId")
	return
}

// GetDeviceDataById retrieves DeviceData by Id. Returns error if
// Id doesn't exist
func GetDeviceDataById(id int64) (v *DeviceData, err error) {
	o := orm.NewOrm()
	v = &DeviceData{Id: id}
	if err = o.QueryTable(new(DeviceData)).Filter("Id", id).RelatedSel().One(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetDeviceDataById retrieves DeviceData by Id. Returns error if
// Id doesn't exist
func GetDeviceTokensForProject(projectId int64) (tokens []string, err error) {
	o := orm.NewOrm()
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("push_tokens.token as token").
		From("devices_data").
		InnerJoin("push_tokens").On("devices_data.id = push_tokens.device_data_id").
		Where("devices_data.project_id = ?")
	sql := qb.String()
	var resultMap []orm.Params
	o.Raw(sql, projectId).Values(&resultMap)
	for _, value := range resultMap {
		tokens = append(tokens, value["token"].(string))
	}
	return tokens, nil
}

// GetAllDeviceData retrieves all DeviceData matches certain condition. Returns empty list if
// no records exist
func GetAllDeviceData(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(DeviceData))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		qs = qs.Filter(k, v)
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []DeviceData
	qs = qs.OrderBy(sortFields...).RelatedSel()
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateDeviceData updates DeviceData by Id and returns error if
// the record to be updated doesn't exist
func UpdateDeviceDataById(m *DeviceData) (err error) {
	o := orm.NewOrm()
	v := DeviceData{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteDeviceData deletes DeviceData by Id and returns error if
// the record to be deleted doesn't exist
func DeleteDeviceData(id int64) (err error) {
	o := orm.NewOrm()
	v := DeviceData{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&DeviceData{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
