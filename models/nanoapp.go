package models

import (
	"errors"
	"fmt"
	"time"

	"github.com/astaxie/beego"

	"github.com/astaxie/beego/orm"
)

// Nanoapp ...
type Nanoapp struct {
	Id                 int64     `orm:"auto;pk" json:"id"`
	Name               string    `json:"name"`
	Description        string    `json:"description"`
	ImageUrl           string    `json:"image_url"`
	BundleUrl          string    `json:"bundle_url"`
	Published          int8      `orm:"default(0)" json:"published"`
	MainComponentName  string    `json:"main_component_name"`
	NanoappType        string    `json:"nanoapp_type"` //ADDON/SDK/CAMPAIGN/ABTEST
	PackageName        string    `json:"package_name"`
	CurrentVersionCode int64     `orm:"default(0)" json:"current_version_code"`
	CurrentVersionName string    `json:"current_version_name"`
	LatestVersionCode  int64     `json:"latest_version_code"`
	LatestVersionName  string    `json:"latest_version_name"`
	CurrentChecksum    string    `json:"current_checksum"`
	LatestChecksum     string    `json:"latest_checksum"`
	CreatedAt          time.Time `orm:"auto_now_add;type(datetime)" json:"created_at"`
	UpdatedAt          time.Time `orm:"auto_now;type(datetime)" json:"updated_at"`
	DeletedAt          time.Time `orm:"auto_now_add;type(datetime)" json:"deleted_at"`
}

// TableName for the developers
func (n *Nanoapp) TableName() string {
	return "nanoapps"
}

func init() {
	orm.RegisterModel(new(Nanoapp))
}

// CreateNanoapp insert a new Nanoapp into database and returns
// last inserted Id on success.
func CreateNanoapp(nanoapp *Nanoapp, project *Project) (*Nanoapp, error) {
	o := orm.NewOrm()
	existingNanoapp, err := GetNanoapp(nanoapp.PackageName)
	if existingNanoapp != nil {
		//num, err := o.QueryTable("nanoapps").Filter("id", nanoapp.Id).Update(nanoapp)
		beego.Info(existingNanoapp.Id)
		beego.Info("Updating existing nanoapp")
		existingNanoapp.Name = nanoapp.Name
		existingNanoapp.Description = nanoapp.Description
		existingNanoapp.PackageName = nanoapp.PackageName
		existingNanoapp.BundleUrl = nanoapp.BundleUrl
		existingNanoapp.ImageUrl = nanoapp.ImageUrl
		existingNanoapp.LatestVersionCode = nanoapp.LatestVersionCode
		existingNanoapp.LatestVersionName = nanoapp.LatestVersionName
		existingNanoapp.LatestChecksum = nanoapp.LatestChecksum
		existingNanoapp.CurrentChecksum = nanoapp.CurrentChecksum
		existingNanoapp.MainComponentName = nanoapp.MainComponentName
		_, err := o.Update(existingNanoapp, 
			"Name", 
			"Description", 
			"PackageName",
			"BundleUrl", 
			"ImageUrl", 
			"LatestVersionCode", 
			"LatestVersionName", 
			"LatestChecksum",
			"CurrentChecksum",
			"MainComponentName")
		if err == nil {
			beego.Info("Update completed")
			return nil, err
		}
		return nil, errors.New("Error while updating nanoapp:" + err.Error())
	}
	beego.Info("Creating new nanoapp")
	_, err = o.Insert(nanoapp)
	if err != nil {
		return nil, errors.New("Error occured while inserting nanoapp")
	}

	nanoappProject := &NanoappProject{}
	nanoappProject.Nanoapp = nanoapp
	nanoappProject.Project = project
	_, err = o.Insert(nanoappProject)
	if err != nil {
		return nil, errors.New("Error occured while inserting nanoapp project")
	}
	beego.Info("Creating new nanoapp complete")
	return nanoapp, err
}

// GetNanoapp package name retrieves
// Id doesn't exist
func GetNanoapp(packageName string) (nanoapp *Nanoapp, err error) {
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("*").
		From("nanoapps").
		Where("package_name = ?").
		Limit(1)
	sql := qb.String()
	o := orm.NewOrm()
	err = o.Raw(sql, packageName).QueryRow(&nanoapp)
	if err == nil {
		return nanoapp, nil
	}
	return nil, err
}

// GetNanoappById retrieves Nanoapp by Id. Returns error if
// Id doesn't exist
func GetNanoappById(id int64) (v *Nanoapp, err error) {
	o := orm.NewOrm()
	v = &Nanoapp{Id: id}
	if err = o.QueryTable(new(Nanoapp)).Filter("Id", id).RelatedSel().One(v); err == nil {
		return v, nil
	}
	return nil, err
}

// UpdateNanoapp updates Nanoapp by Id and returns error if
// the record to be updated doesn't exist
func UpdateNanoappById(m *Nanoapp) (err error) {
	o := orm.NewOrm()
	v := Nanoapp{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteNanoapp deletes Nanoapp by Id and returns error if
// the record to be deleted doesn't exist
func DeleteNanoapp(id int64) (err error) {
	o := orm.NewOrm()
	v := Nanoapp{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&Nanoapp{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
