package models

type DefaultResponse struct {
	Status  string `json:"status"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// DeveloperLogin definiton.
type DeveloperLogin struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// DeveloperLoginInfo
type DeveloperLoginInfo struct {
	Name    string `json:"name"`
	Email   string `json:"email"`
	Token   string `json:"token"`
	Status  string `json:"status"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type ProjectAndNanoapps struct {
	Project
	Nanoapp []Nanoapp `json:"nanoapps"`
}

// DeveloperProjectsResponse
type DeveloperProjectsResponse struct {
	Status             string                `json:"status"`
	Code               int                   `json:"code"`
	Message            string                `json:"message"`
	ProjectAndNanoapps *[]ProjectAndNanoapps `json:"projects"`
}
