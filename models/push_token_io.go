package models

type PushTokenBody struct {
	Token string `json:"push_token"`
}
