package models

import (
	"fmt"
	"time"

	"github.com/astaxie/beego/orm"
)

type Campaign struct {
	Id        int64     `orm:"auto;pk" json:"id"`
	Name      string    `json:"name"`
	Title     string    `json:"title"`
	Subtitle  string    `json:"subtitle"`
	ImageUrl  string    `json:"image_url"`
	Nanoapp   *Nanoapp  `orm:"rel(fk)" json:"nanoapp"`
	CreatedAt time.Time `orm:"auto_now_add;type(datetime)" json:"created_at"`
	UpdatedAt time.Time `orm:"auto_now;type(datetime)" json:"updated_at"`
	DeletedAt time.Time `orm:"auto_now_add;type(datetime)" json:"deleted_at"`
}

// TableName for the developers
func (p *Campaign) TableName() string {
	return "campaigns"
}

func init() {
	orm.RegisterModel(new(Campaign))
}

// AddCampaign insert a new Campaign into database and returns
// last inserted Id on success.
func AddCampaign(m *Campaign) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetCampaignById retrieves Campaign by Id. Returns error if
// Id doesn't exist
func GetCampaignById(id int64) (v *Campaign, err error) {
	o := orm.NewOrm()
	v = &Campaign{Id: id}
	if err = o.QueryTable(new(Campaign)).Filter("Id", id).RelatedSel().One(v); err == nil {
		return v, nil
	}
	return nil, err
}

// UpdateCampaign updates Campaign by Id and returns error if
// the record to be updated doesn't exist
func UpdateCampaignById(m *Campaign) (err error) {
	o := orm.NewOrm()
	v := Campaign{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteCampaign deletes Campaign by Id and returns error if
// the record to be deleted doesn't exist
func DeleteCampaign(id int64) (err error) {
	o := orm.NewOrm()
	v := Campaign{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&Campaign{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
