package models

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/validation"
)

type AbTestVariation struct {
	Id        int64     `orm:"auto;pk" json:"id"`
	Name      string    `json:"name" valid:"Required"`
	TestIndex int       `json:"test_index" valid:"Required"`
	AbTest    *AbTest   `orm:"rel(fk)" json:"ab_test" valid:"Required"`
	Nanoapp   *Nanoapp  `orm:"rel(fk)" json:"nanoapp" valid:"Required"`
	Status    string    `json:"status"`
	CreatedAt time.Time `orm:"auto_now_add;type(datetime)" json:"-"`
	UpdatedAt time.Time `orm:"auto_now;type(datetime)" json:"-"`
	DeletedAt time.Time `orm:"auto_now_add;type(datetime)" json:"-"`
}

func init() {
	orm.RegisterModel(new(AbTestVariation))
}

// TableName for the developers
func (p *AbTestVariation) TableName() string {
	return "ab_tests_variations"
}

// AddAbTestVariation insert a new AbTestVariation into database and returns
// last inserted Id on success.
func AddAbTestVariation(m *AbTestVariation) (id int64, err error) {
	valid := validation.Validation{}
	b, err := valid.Valid(m)
	if err != nil {
		return 0, err
	}
	if !b {
		errorMessages := ""
		for _, err := range valid.Errors {
			errorMessages = errorMessages + " " + err.Key + " " + err.Message
		}
		return 0, errors.New(errorMessages)
	}
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetAbTestVariationById retrieves AbTestVariation by Id. Returns error if
// Id doesn't exist
func GetAbTestVariationById(id int64) (v *AbTestVariation, err error) {
	o := orm.NewOrm()
	v = &AbTestVariation{Id: id}
	if err = o.QueryTable(new(AbTestVariation)).Filter("Id", id).RelatedSel().One(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllAbTestVariation retrieves all AbTestVariation matches certain condition. Returns empty list if
// no records exist
func GetAllAbTestVariation(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(AbTestVariation))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		qs = qs.Filter(k, v)
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []AbTestVariation
	qs = qs.OrderBy(sortFields...).RelatedSel()
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateAbTestVariation updates AbTestVariation by Id and returns error if
// the record to be updated doesn't exist
func UpdateAbTestVariationById(m *AbTestVariation) (err error) {
	o := orm.NewOrm()
	v := AbTestVariation{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteAbTestVariation deletes AbTestVariation by Id and returns error if
// the record to be deleted doesn't exist
func DeleteAbTestVariation(id int64) (err error) {
	o := orm.NewOrm()
	v := AbTestVariation{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&AbTestVariation{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
