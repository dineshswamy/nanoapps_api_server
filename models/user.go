package models

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
	uuid "github.com/satori/go.uuid"
)

type User struct {
	Id                   int64     `orm:"auto;pk"`
	UniqueUserIdentifier string    `json:"unique_user_identifier"`
	Project              *Project  `orm:"rel(fk)"`
	AuthToken            string    `json:"auth_token"`
	CreatedAt            time.Time `orm:"auto_now_add;type(datetime)" json:"created_at"`
	UpdatedAt            time.Time `orm:"auto_now;type(datetime)" json:"updated_at"`
	DeletedAt            time.Time `orm:"auto_now_add;type(datetime)" json:"deleted_at"`
}

// TableName for user ...
func (p *User) TableName() string {
	return "users"
}

func init() {
	orm.RegisterModel(new(User))
}

// AddUser insert a new User into database and returns
// last inserted Id on success.
func AddUser(userCreateInfo UserCreateInfo, deviceDataId int64) (user *User, err error) {
	//Check project id is valid or not
	project, err := GetProjectByToken(userCreateInfo.ProjectIdentifier)
	if err != nil {
		return nil, errors.New("project identifier invalid")
	}
	exist := UserExistAlready(userCreateInfo.UniqueUserId, project.Id)
	if exist {
		return nil, errors.New("user already exists")
	}
	user = &User{}
	user.UniqueUserIdentifier = userCreateInfo.UniqueUserId
	user.Project = &Project{Id: project.Id}
	uuidObj, _ := uuid.NewV4()
	user.AuthToken = uuidObj.String()
	o := orm.NewOrm()
	_, err = o.Insert(user)
	if err != nil {
		return nil, errors.New("error inserting user")
	}
	if err != nil {
		return nil, errors.New("error inserting device")
	}
	deviceData := &DeviceData{Id: deviceDataId}
	userDevice := &UserDeviceData{User: user, DeviceData: deviceData}
	_, err = o.Insert(userDevice)
	if err != nil {
		return nil, errors.New("error inserting user device")
	}
	return user, nil
}

func UserExistAlready(UniqueUserId string, projectId int64) (exist bool) {
	var users []User
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("*").
		From("users").
		Where("project_id = ? AND unique_user_identifier = ?")
	sql := qb.String()
	o := orm.NewOrm()
	o.Raw(sql, projectId, UniqueUserId).QueryRows(&users)
	return len(users) != 0
}

// GetUserById retrieves User by Id. Returns error if
// Id doesn't exist
func GetUserById(id int64) (v *User, err error) {
	o := orm.NewOrm()
	v = &User{Id: id}
	if err = o.QueryTable(new(User)).Filter("Id", id).RelatedSel().One(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllUser retrieves all User matches certain condition. Returns empty list if
// no records exist
func GetAllUser(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(User))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		qs = qs.Filter(k, v)
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []User
	qs = qs.OrderBy(sortFields...).RelatedSel()
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateUser updates User by Id and returns error if
// the record to be updated doesn't exist
func UpdateUserById(m *User) (err error) {
	o := orm.NewOrm()
	v := User{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteUser deletes User by Id and returns error if
// the record to be deleted doesn't exist
func DeleteUser(id int64) (err error) {
	o := orm.NewOrm()
	v := User{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&User{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
