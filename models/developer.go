package models

import (
	"bytes"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
)

// Developer struct
type Developer struct {
	Id               int64     `orm:"auto;pk"`
	Name             string    `json:"name"`
	AccountActivated bool      `orm:"default(true)" json:"account_activated"`
	Email            string    `json:"email"`
	Phone            string    `json:"phone"`
	Password         string    `json:"password"`
	Salt             string    `json:"-"`
	LastLogin        time.Time `orm:"auto_now_add;type(datetime)" json:"last_login"`
	CreatedAt        time.Time `orm:"auto_now_add;type(datetime)" json:"created_at"`
	UpdatedAt        time.Time `orm:"auto_now;type(datetime)" json:"updated_at"`
	DeletedAt        time.Time `orm:"auto_now_add;type(datetime)" json:"deleted_at"`
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// TableName for the developers
func (m *Developer) TableName() string {
	return "developers"
}
func init() {
	orm.RegisterModel(new(Developer))
}

// AddDeveloper insert a new Developer into database and returns
// last inserted Id on success.
func AddDeveloper(m *Developer) (id int64, err error) {
	o := orm.NewOrm()
	developers := CheckDeveloperExists(m)
	var passwordWithSalt bytes.Buffer
	if len(developers) == 0 {
		uuidObj, _ := uuid.NewV4()
		m.Salt = uuidObj.String()
		m.AccountActivated = true
		passwordWithSalt.WriteString(m.Salt)
		passwordWithSalt.WriteString(m.Password)
		m.Password = passwordWithSalt.String()
		m.Password, _ = HashPassword(m.Password)
		id, err = o.Insert(m)
	} else {
		return 0, errors.New("Email or Phone is already in use")
	}
	return id, err
}

//AuthenticateDeveloper Validates a developer credentials and authorises
func AuthenticateDeveloper(developerCreds DeveloperLogin, developer Developer) bool {
	var passwordWithSalt bytes.Buffer
	passwordWithSalt.WriteString(developer.Salt)
	passwordWithSalt.WriteString(developerCreds.Password)
	developerCreds.Password = passwordWithSalt.String()
	return CheckPasswordHash(developerCreds.Password, developer.Password)
}

// CheckDeveloperExists AddDeveloper insert a new Developer into database and returns
// last inserted Id on success.
func CheckDeveloperExists(m *Developer) (developers []Developer) {
	var developerProfiles []Developer
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("id").
		From("developers").
		Where("email = ? OR phone = ?")
	sql := qb.String()
	o := orm.NewOrm()
	o.Raw(sql, m.Email, m.Phone).QueryRows(&developerProfiles)
	return developerProfiles
}

// GetDeveloperById retrieves Developer by Id. Returns error if
// Id doesn't exist
func GetDeveloperById(id int64) (v *Developer, err error) {
	o := orm.NewOrm()
	v = &Developer{Id: id}
	if err = o.QueryTable(new(Developer)).Filter("Id", id).RelatedSel().One(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetDeveloper
func GetDeveloper(attr string, value string) (Developer, error) {
	ormHandler := orm.NewOrm()
	developer := Developer{}
	err := ormHandler.QueryTable(new(Developer)).Filter(attr, value).RelatedSel().One(&developer)
	return developer, err
}

// GetAllDeveloper retrieves all Developer matches certain condition. Returns empty list if
// no records exist
func GetAllDeveloper(query map[string]string, fields []string, sortby []string, order []string,
	offset int64, limit int64) (ml []interface{}, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(Developer))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		qs = qs.Filter(k, v)
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, errors.New("Error: unused 'order' fields")
		}
	}

	var l []Developer
	qs = qs.OrderBy(sortFields...).RelatedSel()
	if _, err = qs.Limit(limit, offset).All(&l, fields...); err == nil {
		if len(fields) == 0 {
			for _, v := range l {
				ml = append(ml, v)
			}
		} else {
			// trim unused fields
			for _, v := range l {
				m := make(map[string]interface{})
				val := reflect.ValueOf(v)
				for _, fname := range fields {
					m[fname] = val.FieldByName(fname).Interface()
				}
				ml = append(ml, m)
			}
		}
		return ml, nil
	}
	return nil, err
}

// UpdateDeveloper updates Developer by Id and returns error if
// the record to be updated doesn't exist
func UpdateDeveloperById(m *Developer) (err error) {
	o := orm.NewOrm()
	v := Developer{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteDeveloper deletes Developer by Id and returns error if
// the record to be deleted doesn't exist
func DeleteDeveloper(id int64) (err error) {
	o := orm.NewOrm()
	v := Developer{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&Developer{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
