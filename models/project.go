package models

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	uuid "github.com/satori/go.uuid"
)

// Developer struct
type Project struct {
	Id                 int64     `orm:"auto;pk" json:"id"`
	Name               string    `json:"name"`
	Os                 string    `json:"os"`
	StoreIdentifier    string    `json:"store_identifier"`
	Token              string    `json:"token"`
	GoogleSenderId     string    `json:"google_sender_id"`
	GoogleServerApiKey string    `json:"google_server_api_key"`
	CreatedAt          time.Time `orm:"auto_now_add;type(datetime)" json:"created_at"`
	UpdatedAt          time.Time `orm:"auto_now;type(datetime)" json:"updated_at"`
	DeletedAt          time.Time `orm:"auto_now_add;type(datetime)" json:"deleted_at"`
}

// TableName for the developers
func (p *Project) TableName() string {
	return "projects"
}

// AddProject insert a new project into database and returns
// last inserted Id on success.
func AddProject(projectDetail *ProjectDetail) (project *Project, err error) {
	o := orm.NewOrm()
	if exist := ProjectExistAlready(projectDetail.StoreIdentifier); exist {
		return nil, errors.New("project identifier already in use")
	}
	project = &Project{}
	project.Name = projectDetail.Name
	project.Os = projectDetail.Os
	project.StoreIdentifier = projectDetail.StoreIdentifier
	uuidObj, _ := uuid.NewV4()
	project.Token = uuidObj.String()
	_, err = o.Insert(project)
	if err != nil {
		return nil, errors.New("Error occured while inserting project")
	}
	developerProject := &DeveloperProject{}
	developerProject.Developer = &Developer{Id: projectDetail.DeveloperId}
	developerProject.Project = &Project{Id: project.Id}
	_, err = o.Insert(developerProject)
	if err != nil {
		return nil, errors.New("Error occured while inserting developer project")
	}

	return project, nil
}

// ProjectExistAlready Checks whether project exists already
func ProjectExistAlready(StoreIdentifier string) bool {
	var projects []Project
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("id").
		From("projects").
		Where("store_identifier = ?")
	sql := qb.String()
	o := orm.NewOrm()
	o.Raw(sql, StoreIdentifier).QueryRows(&projects)
	return len(projects) != 0
}

func init() {
	orm.RegisterModel(new(Project))
}

// GetProjectById retrieves Project by Id. Returns error if
// Id doesn't exist
func GetProjectById(id int64) (v *Project, err error) {
	o := orm.NewOrm()
	v = &Project{Id: id}
	if err = o.QueryTable(new(Project)).Filter("Id", id).RelatedSel().One(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetProjectByToken retrieves Project by Token. Returns error if
// Id doesn't exist
func GetProjectByToken(token string) (v *Project, err error) {
	o := orm.NewOrm()
	v = &Project{Token: token}
	if err = o.QueryTable(new(Project)).Filter("token", token).RelatedSel().One(v); err == nil {
		return v, nil
	}
	return nil, err
}

func VerifyProject(Token string, developerId int) (v *Project, err error) {
	project, err := GetProjectByToken(Token)
	if err != nil {
		return nil, errors.New("Error while verifying project")
	}
	_, err = GetDeveloperProject(developerId, project.Id)
	if err != nil {
		return nil, errors.New("Error while verifying developer project")
	}
	return project, err
}

// UpdateProject updates Project by Id and returns error if
// the record to be updated doesn't exist
func UpdateProjectById(m *Project) (err error) {
	o := orm.NewOrm()
	v := Project{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

func GetNanoappsForProject(projectId int64) (nanoapps []Nanoapp, err error) {
	o := orm.NewOrm()
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("nanoapps.*, nanoapp_projects.project_id").
		From("nanoapp_projects").
		InnerJoin("nanoapps").On("nanoapps.id = nanoapp_projects.nanoapp_id").
		Where("project_id = ?")
	sql := qb.String()
	var resultMap []orm.Params
	o.Raw(sql, projectId).Values(&resultMap)
	for _, value := range resultMap {
		nanoappId, _ := strconv.ParseInt(value["id"].(string), 10, 64)
		published, _ := strconv.ParseInt(value["published"].(string), 0, 64)
		latestVersionCode, _ := strconv.ParseInt(value["latest_version_code"].(string), 10, 64)
		nanoapp := Nanoapp{
			Id:                nanoappId,
			Name:              value["name"].(string),
			Description:       value["description"].(string),
			BundleUrl:         value["bundle_url"].(string),
			ImageUrl:          value["image_url"].(string),
			PackageName:       value["package_name"].(string),
			LatestVersionCode: latestVersionCode,
			MainComponentName: value["main_component_name"].(string),
			LatestVersionName: value["latest_version_name"].(string),
			NanoappType:       value["nanoapp_type"].(string),
			Published:         int8(published),
		}
		nanoapps = append(nanoapps, nanoapp)
	}
	return nanoapps, nil
}

func UpdateProject(updatedProject *Project) (err error) {
	o := orm.NewOrm()
	project := Project{}
	project.Id = updatedProject.Id
	beego.Debug(project)
	if o.Read(&project) == nil {
		project.GoogleSenderId = updatedProject.GoogleSenderId
		project.GoogleServerApiKey = updatedProject.GoogleServerApiKey
		_, err := o.Update(&project, "Token", "GoogleSenderId", "GoogleServerApiKey")
		return err
	}
	return errors.New("Project not found. Check project details")
}
