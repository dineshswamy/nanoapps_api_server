package models

type AbTestAnalyticsRequest struct {
	AbTestVariation *AbTestVariation `json:"ab_test_variation" valid:"Required"`
	StartTime       string           `json:"start_time"`
	EndTime         string           `json:"end_time"`
}
