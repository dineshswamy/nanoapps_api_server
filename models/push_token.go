package models

import (
	"fmt"
	"time"

	"github.com/astaxie/beego/orm"
)

type PushToken struct {
	Id         int64       `orm:"auto;pk"`
	Token      string      `json:"token"`
	DeviceData *DeviceData `orm:"rel(fk)"`
	CreatedAt  time.Time   `orm:"auto_now_add;type(datetime)" json:"created_at"`
	UpdatedAt  time.Time   `orm:"auto_now;type(datetime)" json:"updated_at"`
	DeletedAt  time.Time   `orm:"auto_now_add;type(datetime)" json:"deleted_at"`
}

func init() {
	orm.RegisterModel(new(PushToken))
}

// TableName for the developers
func (p *PushToken) TableName() string {
	return "push_tokens"
}

// AddPushToken insert a new PushToken into database and returns
// last inserted Id on success.
func AddPushToken(m *PushToken) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetPushTokenById retrieves PushToken by Id. Returns error if
// Id doesn't exist
func GetPushTokenById(id int64) (v *PushToken, err error) {
	o := orm.NewOrm()
	v = &PushToken{Id: id}
	if err = o.QueryTable(new(PushToken)).Filter("Id", id).RelatedSel().One(v); err == nil {
		return v, nil
	}
	return nil, err
}

// DeletePushToken deletes PushToken by Id and returns error if
// the record to be deleted doesn't exist
func DeletePushToken(id int64) (err error) {
	o := orm.NewOrm()
	v := PushToken{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&PushToken{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
