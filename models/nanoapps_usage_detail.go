package models

import (
	"sort"
	"time"

	"github.com/astaxie/beego/orm"
)

type NanoappsUsageDetail struct {
	Id        int64     `orm:"auto;pk" json:"id"`
	Nanoapp   *Nanoapp  `orm:"rel(fk)"`
	StartTime time.Time `orm:"type(datetime)" json:"start_time"`
	EndTime   time.Time `orm:"type(datetime)" json:"end_time"`
	CreatedAt time.Time `orm:"auto_now_add;type(datetime)" json:"created_at"`
	UpdatedAt time.Time `orm:"auto_now;type(datetime)" json:"updated_at"`
	DeletedAt time.Time `orm:"auto_now_add;type(datetime)" json:"deleted_at"`
}

// TableName for the developers
func (n *NanoappsUsageDetail) TableName() string {
	return "nanoapp_usage_details"
}

func init() {
	orm.RegisterModel(new(NanoappsUsageDetail))
}

// AddNanoappUsageDetail insert a new NanoappsUsageDetail into database and returns
// last inserted Id on success.
func AddNanoappUsageDetail(m *NanoappsUsageDetail) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

func GetNanoappUsageDetail(nanoappId int64) (int, int, int, error) {
	o := orm.NewOrm()
	usageDetails := []NanoappsUsageDetail{}
	AllDurations := []int{}
	totalDuraton := 0
	_, err := o.QueryTable(new(NanoappsUsageDetail)).Filter("nanoapp_id", nanoappId).OrderBy("created_at").All(&usageDetails)
	if err != nil {
		return 0, 0, 0, err
	}

	for _, usageDetail := range usageDetails {
		duration := usageDetail.EndTime.Sub(usageDetail.StartTime)
		AllDurations = append(AllDurations, int(duration.Nanoseconds()))
		totalDuraton = totalDuraton + int(duration.Nanoseconds())
	}
	sort.Ints(AllDurations)
	longestDuration := AllDurations[len(AllDurations)-1]
	shortestDuration := AllDurations[0]
	averageDuration := totalDuraton / len(AllDurations)
	return longestDuration, shortestDuration, averageDuration, nil
}
