package models

import "time"

type CampaignResponse struct {
	CampaignId int64     `json:"campaign_id"`
	Name       string    `json:"name"`
	Title      string    `json:"title"`
	SubTitle   string    `json:"sub_title"`
	ImageUrl   string    `json:"image_url"`
	NanoappId  int64     `json:"nanoapp_id"`
	CreatedAt  time.Time `orm:"auto_now_add;type(datetime)" json:"created_at"`
}

type InitiateCampaign struct {
	CampaignId int64 `json:"campaign_id"`
	ProjectId  int64 `json:"project_id"`
}
