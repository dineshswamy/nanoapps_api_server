package models

type AddDeviceRequest struct {
	DeviceData   DeviceData `json:"device_data"`
	ProjectToken string     `json:"project_token"`
}

type AddDeviceResponse struct {
	Status          string `json:"status"`
	Code            int    `json:"code"`
	Message         string `json:"message"`
	DeviceAuthToken string `json:"device_auth_token"`
}
