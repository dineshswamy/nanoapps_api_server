package models

// NanoappDetail definiton.
type NanoappDetails struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	ImageUrl    string `json:"image_url"`
	BundleUrl   string `json:"bundle_url"`
	VersionCode string `json:"version_code"`
	VersionName string `json:"version_name"`
	ProjectId   int64  `json:"project_id"`
}

type NanoappCreateResponse struct {
	Status  string   `json:"status"`
	Code    int      `json:"code"`
	Message string   `json:"message"`
	Nanoapp *Nanoapp `json:"nanoapp"`
}

type NanoappUpdate struct {
	Id        int `json:"nanoapp_id"`
	Published int `json:"published"`
}

type NanoappUsageDetails struct {
	NanoappId int64  `json:"nanoapp_id"`
	StartTime string `json:"start_time"`
	EndTime   string `json:"end_time"`
}

type NanoappUsageDetailsBody struct {
	ProjectToken string                `json:"project_token"`
	Details      []NanoappUsageDetails `json:"details"`
}

type ChartCoords struct {
	Date               string `json:"date"`
	NoOfTimesInstalled int    `json:"no_of_times_installed"`
}

type InstallRateResponse struct {
	Status               string        `json:"status"`
	Code                 int           `json:"code"`
	Message              string        `json:"message"`
	ChartCoords          []ChartCoords `json:"chart_coords"`
	TotalInstalls        int           `json:"total_installs"`
	DailyAverageInstalls int           `json:"daily_average_installs"`
}

type NanoappDurationResponse struct {
	Status           string `json:"status"`
	Code             int    `json:"code"`
	Message          string `json:"message"`
	LongestDuration  int    `json:"longest_duration"`
	ShortestDuration int    `json:"shortest_duration"`
	AverageDuration  int    `json:"average_duration"`
}

type UserNanoappBody struct {
	UserId       int64 `json:"user_id"`
	DeviceDataId int64 `json:"device_data_id"`
	NanoappId    int64 `json:"nanoapp_id"`
}

type NanoappDetailsResponse struct {
	Status  string   `json:"status"`
	Code    int      `json:"code"`
	Message string   `json:"message"`
	Nanoapp *Nanoapp `json:"nanoapp"`
}
