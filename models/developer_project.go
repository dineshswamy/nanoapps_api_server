package models

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type DeveloperProject struct {
	Id        int64      `orm:"auto;pk" json:"id"`
	Developer *Developer `orm:"rel(fk)"`
	Project   *Project   `orm:"rel(fk)"`
	CreatedAt time.Time  `orm:"auto_now_add;type(datetime)" json:"created_at"`
	UpdatedAt time.Time  `orm:"auto_now;type(datetime)" json:"updated_at"`
	DeletedAt time.Time  `orm:"auto_now_add;type(datetime)" json:"deleted_at"`
}

// TableName for the developers
func (p *DeveloperProject) TableName() string {
	return "developer_projects"
}

func init() {
	orm.RegisterModel(new(DeveloperProject))
}

// AddDeveloperProject insert a new DeveloperProject into database and returns
// last inserted Id on success.
func AddDeveloperProject(m *DeveloperProject) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetDeveloperProjectById retrieves DeveloperProject by Id. Returns error if
// Id doesn't exist
func GetDeveloperProjectById(id int64) (v *DeveloperProject, err error) {
	o := orm.NewOrm()
	v = &DeveloperProject{Id: id}
	if err = o.QueryTable(new(DeveloperProject)).Filter("Id", id).RelatedSel().One(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetDeveloperProject retrieves DeveloperProject by Id. Returns error if
// Id doesn't exist
func GetDeveloperProject(developerId int, projectId int64) (v *DeveloperProject, err error) {
	var developerProject DeveloperProject
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("id").
		From("developer_projects").
		Where("developer_id = ? AND project_id = ?").
		Limit(1)
	sql := qb.String()
	o := orm.NewOrm()
	o.Raw(sql, developerId, projectId).QueryRow(&developerProject)
	if err == nil {
		return &developerProject, nil
	}
	return nil, err
}

func GetAllDeveloperProjects(developerId int64) (allProjectAndNanoapps []ProjectAndNanoapps, err error) {
	o := orm.NewOrm()
	projectIds := []string{}
	projects := []Project{}
	var resultMap []orm.Params
	qb, _ := orm.NewQueryBuilder("mysql")
	qb.Select("projects.*").
		From("developer_projects").
		InnerJoin("projects").On("projects.id = developer_projects.project_id").
		Where("developer_id = ?")
	sql := qb.String()
	o.Raw(sql, developerId).QueryRows(&projects)
	for _, project := range projects {
		projectIds = append(projectIds, strconv.FormatInt(project.Id, 10))
	}
	qb, _ = orm.NewQueryBuilder("mysql")
	qb.Select("nanoapps.*, nanoapp_projects.project_id").
		From("nanoapp_projects").
		InnerJoin("nanoapps").On("nanoapps.id = nanoapp_projects.nanoapp_id").
		Where("project_id").
		In(strings.Join(projectIds, ","))
	sql = qb.String()
	o.Raw(sql).Values(&resultMap)
	nanoapp := Nanoapp{}
	projectAndNanoapps := ProjectAndNanoapps{}
	//allProjectAndNanoapps := []ProjectAndNanoapps{}
	for _, project := range projects {
		projectAndNanoapps.Project = project
		for _, value := range resultMap {
			projectId := strconv.FormatInt(project.Id, 10)
			if value["project_id"] == projectId {
				nanoappId, _ := strconv.ParseInt(value["id"].(string), 10, 64)
				latestVersionCode, _ := strconv.ParseInt(value["latest_version_code"].(string), 10, 64)
				nanoapp = Nanoapp{
					Id:                nanoappId,
					Name:              value["name"].(string),
					Description:       value["description"].(string),
					BundleUrl:         value["bundle_url"].(string),
					ImageUrl:          value["image_url"].(string),
					PackageName:       value["package_name"].(string),
					LatestVersionCode: latestVersionCode,
					LatestVersionName: value["latest_version_name"].(string),
					NanoappType:       value["nanoapp_type"].(string),
				}
				projectAndNanoapps.Nanoapp = append(projectAndNanoapps.Nanoapp, nanoapp)
			}
		}
		allProjectAndNanoapps = append(allProjectAndNanoapps, projectAndNanoapps)
		//Resetting the variable
		projectAndNanoapps = ProjectAndNanoapps{}
	}

	return allProjectAndNanoapps, err
}

// UpdateDeveloperProjectById updates DeveloperProject by Id and returns error if
// the record to be updated doesn't exist
func UpdateDeveloperProjectById(m *DeveloperProject) (err error) {
	o := orm.NewOrm()
	v := DeveloperProject{Id: m.Id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Update(m); err == nil {
			fmt.Println("Number of records updated in database:", num)
		}
	}
	return
}

// DeleteDeveloperProject deletes DeveloperProject by Id and returns error if
// the record to be deleted doesn't exist
func DeleteDeveloperProject(id int64) (err error) {
	o := orm.NewOrm()
	v := DeveloperProject{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&DeveloperProject{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
