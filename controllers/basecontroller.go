package controllers

import (
	"errors"
	"nanoapps_api_server/models"
	"strings"

	"github.com/astaxie/beego"
	jwt "github.com/dgrijalva/jwt-go"
)

type ControllerError struct {
	Status   int    `json:"status"`
	Code     int    `json:"code"`
	Message  string `json:"message"`
	DevInfo  string `json:"dev_info"`
	MoreInfo string `json:"more_info"`
}

var (
	err404 = &ControllerError{404, 404,
		"Developer or API not found", "Developer or API not found", ""}
)

type BaseController struct {
	beego.Controller
}

// RetError return error information in JSON.
func (base *BaseController) RetError(e *ControllerError) {
	if mode := beego.AppConfig.String("runmode"); mode == "prod" {
		e.DevInfo = ""
	}

	base.Ctx.Output.Header("Content-Type", "application/json; charset=utf-8")
	base.Ctx.ResponseWriter.WriteHeader(e.Status)
	base.Data["json"] = e
	base.ServeJSON()

	base.StopRun()
}

func (base *BaseController) Response(code int, status string, message string) models.DefaultResponse {
	return models.DefaultResponse{Code: code, Status: status, Message: message}
}

// ParseToken parse JWT token in http header.
func (base *BaseController) ParseToken(secret string, header string) (t *jwt.Token, err error) {
	//X-Nanoapp-Token for developers
	//X-Nanoapp-User-Token for users
	//X-Nanoapp-Device-Token for devices
	authString := base.Ctx.Input.Header(header)
	beego.Info("AuthString:", authString)
	kv := strings.Split(authString, " ")
	if len(kv) != 2 || kv[0] != "Bearer" {
		beego.Error("AuthString invalid:", authString)
		return nil, errors.New("AuthString is invalid")
	}
	tokenString := kv[1]

	// Parse token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(secret), nil
	})
	if err != nil {
		beego.Error("Parse token:", err)
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				// That's not even a token
				return nil, errors.New("Malformed token")
			} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
				// Token is either expired or not active yet
				return nil, errors.New("Token expired")
			} else {
				// Couldn't handle this token
				return nil, errors.New("Unknown error. Token cannot be handled")
			}
		} else {
			// Couldn't handle this token
			return nil, errors.New("Unknown error")
		}
	}
	if !token.Valid {
		beego.Error("Token invalid:", tokenString)
		return nil, errors.New("Invalid token")
	}
	return token, nil
}
