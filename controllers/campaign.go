package controllers

import (
	"encoding/json"
	"nanoapps_api_server/helpers"
	"nanoapps_api_server/models"
	"strconv"
)

//  CampaignController operations for Campaign
type CampaignController struct {
	BaseController
}

// URLMapping ...
func (c *CampaignController) URLMapping() {
	c.Mapping("Create", c.Create)
	c.Mapping("Get", c.Get)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
	c.Mapping("StartCampaign", c.StartCampaign)
}

// Post ...
// @Title Post
// @Description create Campaign
// @Param	body		body 	models.Campaign	true		"body for Campaign content"
// @Success 201 {int} models.Campaign
// @Failure 403 body is empty
// @router /create [post]
func (c *CampaignController) Create() {
	var v models.Campaign
	json.Unmarshal(c.Ctx.Input.RequestBody, &v)
	if _, err := models.AddCampaign(&v); err == nil {
		c.Ctx.Output.SetStatus(201)
		c.Data["json"] = v
	} else {
		c.Data["json"] = err.Error()
	}
	c.ServeJSON()
}

// Post ...
// @Title Post
// @Description create Campaign
// @Param	body		body 	models.Campaign	true		"body for Campaign content"
// @Success 201 {int} models.Campaign
// @Failure 403 body is empty
// @router /details/:id [get]
func (c *CampaignController) Get() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	if campaign, err := models.GetCampaignById(id); err == nil {
		c.Ctx.Output.SetStatus(201)
		c.Data["json"] = campaign
	} else {
		c.Data["json"] = err.Error()
	}
	c.ServeJSON()
}

// Start the camapaign
// @Title Start user campaiging
// @Description get Project by user
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Project
// @Failure 403 :id is empty
// @router /start [post]
func (c *CampaignController) StartCampaign() {
	// secret := beego.AppConfig.String("developerJWTSecret")
	// token, err := c.ParseToken(secret, helpers.DEVELOPER_TOKEN)
	// if err != nil {
	// 	c.Ctx.Output.SetStatus(500)
	// 	c.Data["json"] = &models.ProjectResponse{
	// 		Status:  "error",
	// 		Code:    400,
	// 		Message: err.Error(),
	// 		Project: nil,
	// 	}
	// 	c.ServeJSON()
	// 	return
	// }
	// _, ok := token.Claims.(jwt.MapClaims)
	// if !ok {
	// 	c.Ctx.Output.SetStatus(500)
	// 	c.Data["json"] = &models.ProjectResponse{
	// 		Status:  "error",
	// 		Code:    400,
	// 		Message: "Token permission error",
	// 	}
	// 	c.ServeJSON()
	// 	return
	// }
	var initiateCampaign models.InitiateCampaign
	json.Unmarshal(c.Ctx.Input.RequestBody, &initiateCampaign)
	data := map[string]string{
		"purpose":              "campaign_purpose",
		"nanoapp_push_message": "true",
		"campaign_id":          strconv.FormatInt(initiateCampaign.CampaignId, 10),
	}
	tokens, err := models.GetDeviceTokensForProject(initiateCampaign.ProjectId)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{
			Status:  "error",
			Code:    500,
			Message: "Error occured: " + err.Error(),
		}
		c.ServeJSON()
		return
	}
	project, err := models.GetProjectById(initiateCampaign.ProjectId)
	go helpers.SendPushNotification(project.GoogleServerApiKey, tokens, data)
	c.Ctx.Output.SetStatus(200)
	c.Data["json"] = &models.DefaultResponse{
		Status:  "success",
		Code:    200,
		Message: "Campaign successfully started",
	}
	c.ServeJSON()
	return
}
