package controllers

import (
	"encoding/json"
	"nanoapps_api_server/helpers"
	"nanoapps_api_server/models"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	jwt "github.com/dgrijalva/jwt-go"
)

// NanoappController ...
// NanoappController operations for Nanoapp
type NanoappController struct {
	BaseController
}

// URLMapping ...
func (c *NanoappController) URLMapping() {
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("UploadNanoapp", c.UploadNanoapp)
	c.Mapping("AddUsageDetails", c.AddUsageDetails)
	c.Mapping("GetNanoappInstalls", c.GetNanoappInstalls)
	c.Mapping("GetNanoappDetails", c.GetNanoappDetails)
	c.Mapping("GetNanoappDurationDetails", c.GetNanoappDurationDetails)
}

// UploadNanoapp ...
// @Title Post
// @Description create Nanoapp
// @Param	body		body 	models.Nanoapp	true		"body for Nanoapp content"
// @Success 201 {int} models.Nanoapp
// @Failure 403 body is empty
// @router /upload [post]
func (c *NanoappController) UploadNanoapp() {
	//Token verification of the developer
	secret := beego.AppConfig.String("developerJWTSecret")
	token, err := c.ParseToken(secret, helpers.DEVELOPER_TOKEN)
	beego.Info("Started - Token verification of the developer")
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{
			Status:  "error",
			Code:    400,
			Message: err.Error(),
		}
		c.ServeJSON()
		return
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	developerId, _ := claims["id"].(int)
	if !ok {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{
			Status:  "error",
			Code:    400,
			Message: "Token permission error",
		}
		c.ServeJSON()
		return
	}
	//Token verification of the developer complete
	beego.Info("Completed - Token verification of the developer")
	beego.Info("Started - Token verification of the Project")
	//Token verification of the Project
	name := c.GetString("name")
	packageName := c.GetString("package_name")
	versionCode := c.GetString("version_code")
	versionName := c.GetString("version_name")
	description := c.GetString("description")
	projectToken := c.GetString("project_token")
	mainComponentName := c.GetString("main_component_name")
	project, err := models.VerifyProject(projectToken, developerId)
	if err != nil {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = &models.ProjectResponse{
			Status:  "error",
			Code:    400,
			Message: err.Error(),
		}
		c.ServeJSON()
		return
	}
	beego.Info("Completed - Token verification of the Project")
	beego.Info("Started - Processing bundle file")
	_, fileHeader, err := c.GetFile("bundle_file")
	path := []string{}
	path = append(path, "nanoapps")
	path = append(path, project.StoreIdentifier)
	path = append(path, packageName)
	path = append(path, versionCode)
	filePath := strings.Join(path, "/") + "/"

	beego.Info("Bundle file path..")
	beego.Info(filePath)
	checksum, err := helpers.UploadFile(fileHeader, filePath, "bundle")
	beego.Info("Calculating file checksum")

	beego.Info("Comparing checksums ...")
	beego.Info("Checksum = " + checksum)
	if checksum != c.GetString("checksum") {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Message: "Checksum verification failed: " + err.Error(), Code: 400}
		c.ServeJSON()
		return
	}
	beego.Info("Completed - Processing bundle file")
	beego.Info("Started - Processing image file")
	_, fileHeader, err = c.GetFile("app_icon")
	appIconFile := "app_icon" + filepath.Ext(fileHeader.Filename)
	beego.Info("App Icon file path..")
	beego.Info(appIconFile)
	_, err = helpers.UploadFile(fileHeader, filePath, appIconFile)
	beego.Info("Completed - Processing image file")
	beego.Info("Completed - Processing files")

	nanoapp := &models.Nanoapp{}
	nanoapp.Name = name
	nanoapp.PackageName = packageName
	nanoapp.LatestVersionCode, err = strconv.ParseInt(versionCode, 10, 64)
	beego.Info("Latest version code");
	beego.Info(nanoapp.LatestVersionCode);
	nanoapp.LatestVersionName = versionName
	nanoapp.Description = description
	nanoapp.MainComponentName = mainComponentName
	nanoapp.LatestChecksum = checksum
	nanoapp.CurrentChecksum = checksum
	nanoapp.ImageUrl = filePath + appIconFile
	nanoapp.BundleUrl = filePath + "bundle"
	nanoapp, err = models.CreateNanoapp(nanoapp, project)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Message: err.Error(), Code: 400}
		c.ServeJSON()
		return
	}
	c.Ctx.Output.SetStatus(201)
	c.Data["json"] = &models.DefaultResponse{Status: "success", Message: "Successfully copied files", Code: 200}
	c.ServeJSON()
}

// AddUsageDetails ...
// @Title Post
// @Description create Nanoapp
// @Param	body		body 	models.Nanoapp	true		"body for Nanoapp content"
// @Success 201 {int} models.Nanoapp
// @Failure 403 body is empty
// @router /add_usage_details [post]
func (c *NanoappController) AddUsageDetails() {
	// secret := beego.AppConfig.String("deviceJWTSecret")
	// token, err := c.ParseToken(secret, helpers.DEVICE_TOKEN)
	// if err != nil {
	// 	c.Ctx.Output.SetStatus(500)
	// 	c.Data["json"] = &models.DefaultResponse{
	// 		Status:  "error",
	// 		Code:    400,
	// 		Message: err.Error(),
	// 	}
	// 	c.ServeJSON()
	// 	return
	// }
	// claims, ok := token.Claims.(jwt.MapClaims)
	// deviceDataId, _ := strconv.ParseInt(claims["device_data_id"].(string), 10, 64)
	// if !ok || deviceDataId == 0 {
	// 	c.Ctx.Output.SetStatus(500)
	// 	c.Data["json"] = &models.ProjectResponse{
	// 		Status:  "error",
	// 		Code:    400,
	// 		Message: "Token permission error or device data id is invalid",
	// 		Project: nil,
	// 	}
	// 	c.ServeJSON()
	// 	return
	// }
	var nanoappUsageDetailsBody models.NanoappUsageDetailsBody
	json.Unmarshal(c.Ctx.Input.RequestBody, &nanoappUsageDetailsBody)
	projectToken := nanoappUsageDetailsBody.ProjectToken
	project, err := models.GetProjectByToken(projectToken)
	if err != nil || project.Id == 0 {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Message: "Project token invalid", Code: 500}
		c.ServeJSON()
		return
	}
	for _, nanoappUsageDetail := range nanoappUsageDetailsBody.Details {
		startTimeStr := nanoappUsageDetail.StartTime
		endTimeStr := nanoappUsageDetail.EndTime
		nanoappId := nanoappUsageDetail.NanoappId
		format := "2006-01-02 15:04:05"
		startTime, err := time.Parse(format, startTimeStr)
		if err != nil {
			c.Ctx.Output.SetStatus(500)
			c.Data["json"] = &models.DefaultResponse{Status: "failure",
				Message: "Time formatting error " + err.Error(), Code: 500}
			c.ServeJSON()
			return
		}
		endTime, err := time.Parse(format, endTimeStr)
		if err != nil {
			c.Ctx.Output.SetStatus(500)
			c.Data["json"] = &models.DefaultResponse{Status: "failure",
				Message: "Time formatting error " + err.Error(), Code: 500}
			c.ServeJSON()
			return
		}
		nanoappUsageDetail := models.NanoappsUsageDetail{
			Nanoapp:   &models.Nanoapp{Id: nanoappId},
			StartTime: startTime,
			EndTime:   endTime,
		}
		_, err = models.AddNanoappUsageDetail(&nanoappUsageDetail)
		if err != nil {
			c.Ctx.Output.SetStatus(500)
			c.Data["json"] = &models.DefaultResponse{Status: "failure",
				Message: "Error while adding nanoapp usage detail " + err.Error(), Code: 500}
			c.ServeJSON()
			return
		}
	}
	c.Data["json"] = &models.DefaultResponse{Status: "success",
		Code: 200, Message: "Added nanoapp usage detail successfully"}
	c.ServeJSON()
}

// GetNanoappDetails ...
// @Title Post
// @Description create Nanoapp
// @Param	body		body 	models.Nanoapp	true		"body for Nanoapp content"
// @Success 201 {int} models.Nanoapp
// @Failure 403 body is empty
// @router /get_nanoapp_details [get]
func (c *NanoappController) GetNanoappDetails() {
	//Verifiying device token
	// secret := beego.AppConfig.String("deviceJWTSecret")
	// token, err := c.ParseToken(secret, helpers.DEVICE_TOKEN)
	// if err != nil {
	// 	c.Ctx.Output.SetStatus(500)
	// 	c.Data["json"] = &models.DefaultResponse{
	// 		Status:  "error",
	// 		Code:    400,
	// 		Message: err.Error(),
	// 	}
	// 	c.ServeJSON()
	// 	return
	// }
	// claims, ok := token.Claims.(jwt.MapClaims)
	// deviceDataId, _ := strconv.ParseInt(claims["device_data_id"].(string), 10, 64)
	// if !ok || deviceDataId == 0 {
	// 	c.Ctx.Output.SetStatus(500)
	// 	c.Data["json"] = &models.ProjectResponse{
	// 		Status:  "error",
	// 		Code:    400,
	// 		Message: "Token permission error or device data id is invalid",
	// 		Project: nil,
	// 	}
	// 	c.ServeJSON()
	// 	return
	// }
	packageName := c.GetString("package_name")
	beego.Debug("Package name = ")
	beego.Debug(packageName)

	nanoapp, err := models.GetNanoapp(packageName)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Message: "Error while fetching nanoapp details" + err.Error(), Code: 500}
		c.ServeJSON()
		return
	}
	c.Ctx.Output.SetStatus(200)
	c.Data["json"] = &models.NanoappDetailsResponse{
		Status:  "success",
		Message: "Successfully fetched nanoapp",
		Code:    200,
		Nanoapp: nanoapp,
	}
	c.ServeJSON()
}

// InstallRate ...
// @Title Post
// @Description create Nanoapp
// @Param	body		body 	models.Nanoapp	true		"body for Nanoapp content"
// @Success 201 {int} models.Nanoapp
// @Failure 403 body is empty
// @router /add_install [post]
func (c *NanoappController) AddNanoappInstall() {
	//Have to add developer token check
	var userNanoappBody models.UserNanoappBody
	json.Unmarshal(c.Ctx.Input.RequestBody, &userNanoappBody)
	userId := userNanoappBody.UserId
	deviceDataId := userNanoappBody.DeviceDataId
	nanoappId := userNanoappBody.NanoappId
	_, err := models.AddUserNanoapp(&models.UserNanoapp{
		User:       &models.User{Id: userId},
		DeviceData: &models.DeviceData{Id: deviceDataId},
		Nanoapp:    &models.Nanoapp{Id: nanoappId},
	})
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Message: "Error while adding nanoapp install rate " + err.Error(), Code: 500}
		c.ServeJSON()
		return
	}
	c.Ctx.Output.SetStatus(200)
	c.Data["json"] = &models.DefaultResponse{
		Status:  "success",
		Message: "Successfully added install rate",
		Code:    200,
	}
	c.ServeJSON()
}

// InstallRate ...
// @Title Post
// @Description create Nanoapp
// @Param	body		body 	models.Nanoapp	true		"body for Nanoapp content"
// @Success 201 {int} models.Nanoapp
// @Failure 403 body is empty
// @router /:nanoapp_id/install_rate [get]
func (c *NanoappController) GetNanoappInstalls() {
	idStr := c.Ctx.Input.Param(":nanoapp_id")
	nanoappId, err := strconv.ParseInt(idStr, 10, 64)
	installs, err := models.GetInstallRateForNanoapp(nanoappId)
	totalInstalls := len(installs)
	graphCoords := make(map[string]int)
	for _, install := range installs {
		date := install.CreatedAt.Format("2 Jan 06")
		val, ok := graphCoords[date]
		if ok {
			val += 1
		} else {
			val = 1
		}
		graphCoords[date] = val
	}
	chartCoords := []models.ChartCoords{}
	chartCoord := models.ChartCoords{}
	totalInstallsDaily := 0
	for key, value := range graphCoords {
		chartCoord = models.ChartCoords{
			Date:               key,
			NoOfTimesInstalled: value,
		}
		totalInstallsDaily = totalInstallsDaily + value
		chartCoords = append(chartCoords, chartCoord)
	}
	dailyAverageInstalls := 0
	if len(graphCoords) != 0 {
		dailyAverageInstalls = totalInstallsDaily / len(graphCoords)
	}
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Message: "Error while retrieving install rate " + err.Error(), Code: 500}
		c.ServeJSON()
		return
	}
	c.Ctx.Output.SetStatus(200)
	c.Data["json"] = &models.InstallRateResponse{
		Status:               "success",
		Message:              "Successfully fetched install rate",
		Code:                 200,
		ChartCoords:          chartCoords,
		TotalInstalls:        totalInstalls,
		DailyAverageInstalls: dailyAverageInstalls,
	}
	c.ServeJSON()
}

// InstallRate ...
// @Title Post
// @Description create Nanoapp
// @Param	body		body 	models.Nanoapp	true		"body for Nanoapp content"
// @Success 201 {int} models.Nanoapp
// @Failure 403 body is empty
// @router /:nanoapp_id/time_duration [get]
func (c *NanoappController) GetNanoappDurationDetails() {
	idStr := c.Ctx.Input.Param(":nanoapp_id")
	nanoappId, err := strconv.ParseInt(idStr, 10, 64)
	long, short, average, err := models.GetNanoappUsageDetail(nanoappId)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Message: "Error while retrieving duration details " + err.Error(), Code: 500}
		c.ServeJSON()
		return
	}
	c.Ctx.Output.SetStatus(200)
	c.Data["json"] = &models.NanoappDurationResponse{
		Status:           "success",
		Message:          "Successfully fetched install rate",
		Code:             200,
		LongestDuration:  long,
		ShortestDuration: short,
		AverageDuration:  average,
	}
	c.ServeJSON()
}

// GetOne ...
// @Title Get One
// @Description get Nanoapp by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Nanoapp
// @Failure 403 :id is empty
// @router /:id [get]
func (c *NanoappController) GetOne() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	v, err := models.GetNanoappById(id)
	if err != nil {
		c.Data["json"] = err.Error()
	} else {
		c.Data["json"] = v
	}
	c.ServeJSON()
}

// PublishNanoapp ...
// @Title Get One
// @Description get Nanoapp by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Nanoapp
// @Failure 403 :id is empty
// @router /update [post]
func (c *NanoappController) UpdateNanoapp() {
	secret := beego.AppConfig.String("developerJWTSecret")
	_, err := c.ParseToken(secret, helpers.DEVELOPER_TOKEN)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{
			Status:  "error",
			Code:    400,
			Message: err.Error(),
		}
		c.ServeJSON()
		return
	}
	var nanoapp models.Nanoapp
	json.Unmarshal(c.Ctx.Input.RequestBody, &nanoapp)
	o := orm.NewOrm()
	if err = o.Read(&models.Nanoapp{Id: nanoapp.Id}); err == nil {
		if _, err := o.Update(&nanoapp, "NanoappType"); err == nil {
			c.Data["json"] = &models.DefaultResponse{Status: "success",
				Code: 200, Message: "Updated nanoapp successfully"}
		} else {
			c.Data["json"] = &models.DefaultResponse{Status: "failure",
				Code: 400, Message: "Error occured" + err.Error()}
		}
	} else {
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Code: 400, Message: "Error occured" + err.Error()}
	}

	c.ServeJSON()
}
