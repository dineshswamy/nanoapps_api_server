package controllers

import (
	"encoding/json"
	"errors"
	"nanoapps_api_server/helpers"
	"nanoapps_api_server/models"
	"strconv"
	"strings"

	"github.com/astaxie/beego"
	jwt "github.com/dgrijalva/jwt-go"
)

//  UserController operations for User
type UserController struct {
	BaseController
}

type HelloMessage struct {
	Message string `json:"message"`
}

// URLMapping ...
func (c *UserController) URLMapping() {
	c.Mapping("Create", c.Create)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("PrintHello", c.PrintHello)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// Post ...
// @Title Post
// @Description create User
// @Param	body		body 	models.User	true		"body for User content"
// @Success 201 {int} models.User
// @Failure 403 body is empty
// @router /create [post]
func (c *UserController) Create() {
	var userInfo models.UserCreateInfo
	json.Unmarshal(c.Ctx.Input.RequestBody, &userInfo)
	secret := beego.AppConfig.String("deviceJWTSecret")
	token, err := c.ParseToken(secret, helpers.DEVICE_TOKEN)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{
			Status:  "error",
			Code:    400,
			Message: err.Error(),
		}
		c.ServeJSON()
		return
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	deviceDataId, _ := strconv.ParseInt(claims["device_data_id"].(string), 10, 64)
	if !ok {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.ProjectResponse{
			Status:  "error",
			Code:    400,
			Message: "Token permission error",
			Project: nil,
		}
		c.ServeJSON()
		return
	}
	user, err := models.AddUser(userInfo, deviceDataId)
	//Issue a user token
	if err == nil {
		claims := make(jwt.MapClaims)
		claims["user_id"] = strconv.FormatInt(user.Id, 10)
		token = jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		secret = beego.AppConfig.String("userJWTSecret")
		tokenString, err := token.SignedString([]byte(secret))
		if err != nil {
			beego.Error("jwt.SignedString:", err)
			return
		}
		c.Ctx.Output.SetStatus(201)
		c.Data["json"] = &models.UserCreateInfoResponse{
			Status:    "success",
			Code:      201,
			Message:   "User created successfully",
			UserToken: tokenString,
		}
	} else {
		c.Data["json"] = c.Response(200, "failure", err.Error())
	}
	c.ServeJSON()
}

// GetOne ...
// @Title Get One
// @Description get User by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.User
// @Failure 403 :id is empty
// @router /:id [get]
func (c *UserController) GetOne() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	v, err := models.GetUserById(id)
	if err != nil {
		c.Data["json"] = err.Error()
	} else {
		c.Data["json"] = v
	}
	c.ServeJSON()
}

// GetOne ...
// @Title Get One
// @Description get User by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.User
// @Failure 403 :id is empty
// @router /hello/:id [get]
func (c *UserController) PrintHello() {
	idStr := c.Ctx.Input.Param(":id")
	c.Data["json"] = &HelloMessage{Message: "Hi.. Hello Mentee.. " + idStr}
	c.ServeJSON()
}

// GetAll ...
// @Title Get All
// @Description get User
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} models.User
// @Failure 403
// @router / [get]
func (c *UserController) GetAll() {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = errors.New("Error: invalid query key/value pair")
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAllUser(query, fields, sortby, order, offset, limit)
	if err != nil {
		c.Data["json"] = err.Error()
	} else {
		c.Data["json"] = l
	}
	c.ServeJSON()
}

// Put ...
// @Title Put
// @Description update the User
// @Param	id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.User	true		"body for User content"
// @Success 200 {object} models.User
// @Failure 403 :id is not int
// @router /:id [put]
func (c *UserController) Put() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	v := models.User{Id: id}
	json.Unmarshal(c.Ctx.Input.RequestBody, &v)
	if err := models.UpdateUserById(&v); err == nil {
		c.Data["json"] = "OK"
	} else {
		c.Data["json"] = err.Error()
	}
	c.ServeJSON()
}

// Delete ...
// @Title Delete
// @Description delete the User
// @Param	id		path 	string	true		"The id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 id is empty
// @router /:id [delete]
func (c *UserController) Delete() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	if err := models.DeleteUser(id); err == nil {
		c.Data["json"] = "OK"
	} else {
		c.Data["json"] = err.Error()
	}
	c.ServeJSON()
}
