package controllers

import (
	"encoding/json"
	"errors"
	"nanoapps_api_server/models"
	"strconv"
	"strings"
	"time"

	"github.com/astaxie/beego"
	"github.com/dgrijalva/jwt-go"
)

//  DeveloperController operations for Developer
type DeveloperController struct {
	BaseController
}

// Post ...
// @Title Registration api for developer
// @Description create Developer
// @Param	body		body 	models.Developer	true		"body for Developer content"
// @Success 201 {int} models.Developer
// @Failure 403 body is empty
// @router /register [post]
func (c *DeveloperController) Register() {
	var developer models.Developer
	json.Unmarshal(c.Ctx.Input.RequestBody, &developer)
	developerId, err := models.AddDeveloper(&developer)
	if err == nil {
		claims := make(jwt.MapClaims)
		claims["id"] = strconv.FormatInt(developerId, 10)
		claims["name"] = developer.Name
		claims["email"] = developer.Email
		claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		// Sign and get the complete encoded token as a string
		secret := beego.AppConfig.String("developerJWTSecret")
		tokenString, err := token.SignedString([]byte(secret))
		if err != nil {
			beego.Error("jwt.SignedString:", err)
			return
		}
		c.Ctx.Output.SetStatus(201)
		c.Data["json"] = models.DeveloperLoginInfo{
			Name:    developer.Name,
			Email:   developer.Email,
			Token:   tokenString,
			Status:  "success",
			Code:    200,
			Message: "Registration Successful"}
	} else {
		c.Ctx.Output.SetStatus(200)
		c.Data["json"] = models.DeveloperLoginInfo{
			Status:  "failure",
			Code:    200,
			Message: err.Error()}
	}
	c.ServeJSON()
}

// Post ...
// @Title Login for developers
// @Description login Developer
// @Param	body		body 	models.Developer	true		"body for Developer content"
// @Success 201 {int} models.Developer
// @Failure 403 body is empty
// @router /login [post]
func (c *DeveloperController) Login() {
	var developerCreds models.DeveloperLogin
	json.Unmarshal(c.Ctx.Input.RequestBody, &developerCreds)
	developer, _ := models.GetDeveloper("email", developerCreds.Email)
	authenticated := models.AuthenticateDeveloper(developerCreds, developer)
	if authenticated {
		claims := make(jwt.MapClaims)
		claims["id"] = strconv.FormatInt(developer.Id, 10)
		claims["name"] = developer.Name
		claims["email"] = developer.Email
		claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		// Sign and get the complete encoded token as a string
		secret := beego.AppConfig.String("developerJWTSecret")
		tokenString, err := token.SignedString([]byte(secret))
		if err != nil {
			beego.Error("jwt.SignedString:", err)
			return
		}
		c.Ctx.Output.SetStatus(201)
		c.Data["json"] = models.DeveloperLoginInfo{
			Name:    developer.Name,
			Email:   developer.Email,
			Token:   tokenString,
			Status:  "success",
			Code:    200,
			Message: "successfully logged in"}
	} else {
		c.Data["json"] = c.Response(200, "failure", "Email or password is wrong")
	}
	c.ServeJSON()
}

// GetAll ...
// @Title Get All
// @Description get Developer
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} models.Developer
// @Failure 403
// @router / [get]
func (c *DeveloperController) GetAll() {
	var fields []string
	var sortby []string
	var order []string
	var query = make(map[string]string)
	var limit int64 = 10
	var offset int64

	// fields: col1,col2,entity.col3
	if v := c.GetString("fields"); v != "" {
		fields = strings.Split(v, ",")
	}
	// limit: 10 (default is 10)
	if v, err := c.GetInt64("limit"); err == nil {
		limit = v
	}
	// offset: 0 (default is 0)
	if v, err := c.GetInt64("offset"); err == nil {
		offset = v
	}
	// sortby: col1,col2
	if v := c.GetString("sortby"); v != "" {
		sortby = strings.Split(v, ",")
	}
	// order: desc,asc
	if v := c.GetString("order"); v != "" {
		order = strings.Split(v, ",")
	}
	// query: k:v,k:v
	if v := c.GetString("query"); v != "" {
		for _, cond := range strings.Split(v, ",") {
			kv := strings.SplitN(cond, ":", 2)
			if len(kv) != 2 {
				c.Data["json"] = errors.New("Error: invalid query key/value pair")
				c.ServeJSON()
				return
			}
			k, v := kv[0], kv[1]
			query[k] = v
		}
	}

	l, err := models.GetAllDeveloper(query, fields, sortby, order, offset, limit)
	if err != nil {
		c.Data["json"] = err.Error()
	} else {
		c.Data["json"] = l
	}
	c.ServeJSON()
}

// Put ...
// @Title Put
// @Description update the Developer
// @Param	id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.Developer	true		"body for Developer content"
// @Success 200 {object} models.Developer
// @Failure 403 :id is not int
// @router /:id [put]
func (c *DeveloperController) Put() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	v := models.Developer{Id: id}
	json.Unmarshal(c.Ctx.Input.RequestBody, &v)
	if err := models.UpdateDeveloperById(&v); err == nil {
		c.Data["json"] = "OK"
	} else {
		c.Data["json"] = err.Error()
	}
	c.ServeJSON()
}

// Delete ...
// @Title Delete
// @Description delete the Developer
// @Param	id		path 	string	true		"The id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 id is empty
// @router /:id [delete]
func (c *DeveloperController) Delete() {
	idStr := c.Ctx.Input.Param(":id")
	id, _ := strconv.ParseInt(idStr, 0, 64)
	if err := models.DeleteDeveloper(id); err == nil {
		c.Data["json"] = "OK"
	} else {
		c.Data["json"] = err.Error()
	}
	c.ServeJSON()
}
