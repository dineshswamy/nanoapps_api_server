package controllers

import (
	"encoding/json"
	"nanoapps_api_server/models"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/validation"
)

//  AbtestController operations for Abtest
type AbtestController struct {
	beego.Controller
}

// URLMapping ...
func (c *AbtestController) URLMapping() {
	c.Mapping("Create", c.Create)
	c.Mapping("AddVariation", c.AddVariation)
}

// Post ...
// @Title Post
// @Description create Abtest
// @Param	body		body 	models.Abtest	true		"body for Abtest content"
// @Success 201 {int} models.Abtest
// @Failure 403 body is empty
// @router /create [post]
func (c *AbtestController) Create() {
	var v models.AbTest
	json.Unmarshal(c.Ctx.Input.RequestBody, &v)
	if _, err := models.AddAbTest(&v); err == nil {
		c.Ctx.Output.SetStatus(201)
		c.Data["json"] = v
	} else {
		c.Data["json"] = err.Error()
	}
	c.ServeJSON()
}

// Post ...
// @Title Post
// @Description Add variation
// @Success 201 {int} models.Abtest
// @Failure 403 body is empty
// @router /add_variation [post]
func (c *AbtestController) AddVariation() {
	var v models.AbTestVariation
	json.Unmarshal(c.Ctx.Input.RequestBody, &v)
	if _, err := models.AddAbTestVariation(&v); err == nil {
		c.Ctx.Output.SetStatus(201)
		c.Data["json"] = v
	} else {
		c.Data["json"] = err.Error()
	}
	c.ServeJSON()
}

// Post ...
// @Title Post
// @Description Add variation
// @Success 201 {int} models.Abtest
// @Failure 403 body is empty
// @router /add_analytics [post]
func (c *AbtestController) AddAbTestAnalytics() {
	var abtestAnalyticsRequest models.AbTestAnalyticsRequest
	json.Unmarshal(c.Ctx.Input.RequestBody, &abtestAnalyticsRequest)
	valid := validation.Validation{}
	is_valid, err := valid.Valid(abtestAnalyticsRequest)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Message: "Validation error " + err.Error(), Code: 500}
		c.ServeJSON()
		return
	}
	if !is_valid {
		errorMessages := ""
		for _, err := range valid.Errors {
			errorMessages = errorMessages + " " + err.Key + " " + err.Message
		}
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Message: "Validation error " + err.Error(), Code: 500}
		c.ServeJSON()
		return
	}
	//Converting string to time object
	startTimeStr := abtestAnalyticsRequest.StartTime
	endTimeStr := abtestAnalyticsRequest.EndTime
	format := "2006-01-02 15:04:05"
	startTime, err := time.Parse(format, startTimeStr)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Message: "Time formatting error " + err.Error(), Code: 500}
		c.ServeJSON()
		return
	}
	endTime, err := time.Parse(format, endTimeStr)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Message: "Time formatting error " + err.Error(), Code: 500}
		c.ServeJSON()
		return
	}
	abtestAnalytics := models.AbTestAnalytics{
		AbTestVariation: abtestAnalyticsRequest.AbTestVariation,
		StartTime:       startTime,
		EndTime:         endTime,
	}
	if _, err := models.AddAbTestAnalytics(&abtestAnalytics); err == nil {
		c.Ctx.Output.SetStatus(201)
		c.Data["json"] = abtestAnalytics
	} else {
		c.Data["json"] = err.Error()
	}
	c.ServeJSON()
}
