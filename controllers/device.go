package controllers

import (
	"encoding/json"
	"nanoapps_api_server/helpers"
	"nanoapps_api_server/models"
	"strconv"

	"github.com/astaxie/beego"
	jwt "github.com/dgrijalva/jwt-go"
)

// DeviceController operations for Device
type DeviceController struct {
	BaseController
}

// URLMapping ...
func (c *DeviceController) URLMapping() {
	c.Mapping("AddDevice", c.AddDevice)
	c.Mapping("GetOne", c.GetOne)
	c.Mapping("GetAll", c.GetAll)
	c.Mapping("Put", c.Put)
	c.Mapping("Delete", c.Delete)
}

// Post ...
// @Title Create
// @Description create Device
// @Param	body		body 	models.Device	true		"body for Device content"
// @Success 201 {object} models.Device
// @Failure 403 body is empty
// @router /add [post]
func (c *DeviceController) AddDevice() {
	var deviceDataRequest models.AddDeviceRequest
	json.Unmarshal(c.Ctx.Input.RequestBody, &deviceDataRequest)
	projectToken := deviceDataRequest.ProjectToken
	project, err := models.GetProjectByToken(projectToken)
	if err != nil || project.Id == 0 {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Message: "Project token invalid", Code: 500}
		c.ServeJSON()
		return
	}
	deviceData := deviceDataRequest.DeviceData
	deviceData.ProjectId = project.Id
	_, device_data_id, err := models.AddDeviceData(&deviceData)
	if err == nil {
		claims := make(jwt.MapClaims)
		claims["device_data_id"] = strconv.FormatInt(device_data_id, 10)
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		// Sign and get the complete encoded token as a string
		secret := beego.AppConfig.String("deviceJWTSecret")
		tokenString, err := token.SignedString([]byte(secret))
		if err != nil {
			beego.Error("jwt.SignedString:", err)
			return
		}
		c.Data["json"] = &models.AddDeviceResponse{
			Status:          "success",
			Code:            201,
			Message:         "Device created successfully",
			DeviceAuthToken: tokenString,
		}
		c.Ctx.Output.SetStatus(201)
	} else {
		c.Data["json"] = c.Response(200, "failure", err.Error())
	}
	c.ServeJSON()
}

// Add push token ...
// @Title Create
// @Description create Device
// @Param	body		body 	models.Device	true		"body for Device content"
// @Success 201 {object} models.Device
// @Failure 403 body is empty
// @router /add_push_token [post]
func (c *DeviceController) AddPushToken() {
	//Verify API request from user device
	secret := beego.AppConfig.String("deviceJWTSecret")
	token, err := c.ParseToken(secret, helpers.DEVICE_TOKEN)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{
			Status:  "error",
			Code:    400,
			Message: err.Error(),
		}
		c.ServeJSON()
		return
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	device_data_id, _ := strconv.ParseInt(claims["device_data_id"].(string), 10, 64)
	if !ok {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.ProjectResponse{
			Status:  "error",
			Code:    400,
			Message: "Token permission error",
			Project: nil,
		}
		c.ServeJSON()
		return
	}
	var pushTokenBody models.PushTokenBody
	json.Unmarshal(c.Ctx.Input.RequestBody, &pushTokenBody)
	deviceData := models.DeviceData{Id: device_data_id}
	pushToken := models.PushToken{Token: pushTokenBody.Token, DeviceData: &deviceData}
	_, err = models.AddPushToken(&pushToken)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{
			Status:  "error",
			Code:    400,
			Message: "Error inserting push token " + err.Error(),
		}
		c.ServeJSON()
		return
	}
	c.Ctx.Output.SetStatus(201)
	c.Data["json"] = &models.DefaultResponse{
		Status:  "success",
		Code:    201,
		Message: "Push token added successfully",
	}
	c.ServeJSON()
}

// GetOne ...
// @Title GetOne
// @Description get Device by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Device
// @Failure 403 :id is empty
// @router /:id [get]
func (c *DeviceController) GetOne() {

}

// GetAll ...
// @Title GetAll
// @Description get Device
// @Param	query	query	string	false	"Filter. e.g. col1:v1,col2:v2 ..."
// @Param	fields	query	string	false	"Fields returned. e.g. col1,col2 ..."
// @Param	sortby	query	string	false	"Sorted-by fields. e.g. col1,col2 ..."
// @Param	order	query	string	false	"Order corresponding to each sortby field, if single value, apply to all sortby fields. e.g. desc,asc ..."
// @Param	limit	query	string	false	"Limit the size of result set. Must be an integer"
// @Param	offset	query	string	false	"Start position of result set. Must be an integer"
// @Success 200 {object} models.Device
// @Failure 403
// @router / [get]
func (c *DeviceController) GetAll() {

}

// Put ...
// @Title Put
// @Description update the Device
// @Param	id		path 	string	true		"The id you want to update"
// @Param	body		body 	models.Device	true		"body for Device content"
// @Success 200 {object} models.Device
// @Failure 403 :id is not int
// @router /:id [put]
func (c *DeviceController) Put() {

}

// Delete ...
// @Title Delete
// @Description delete the Device
// @Param	id		path 	string	true		"The id you want to delete"
// @Success 200 {string} delete success!
// @Failure 403 id is empty
// @router /:id [delete]
func (c *DeviceController) Delete() {

}
