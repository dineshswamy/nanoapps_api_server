package controllers

import (
	"encoding/json"
	"nanoapps_api_server/helpers"
	"nanoapps_api_server/models"
	"strconv"

	"github.com/astaxie/beego"
	jwt "github.com/dgrijalva/jwt-go"
)

//  ProjectController operations for Project
type ProjectController struct {
	BaseController
}

// URLMapping ...
func (c *ProjectController) URLMapping() {
	c.Mapping("Create", c.Create)
	c.Mapping("VerifyProjectToken", c.VerifyProjectToken)
	c.Mapping("GetProjectsOfDev", c.GetProjectsOfDev)
	c.Mapping("GetNanoappsOfProject", c.GetNanoappsOfProject)
	c.Mapping("UpdateProject", c.UpdateProject)
}

// Create ...
// @Title Create
// @Description create Project
// @Param	body		body 	models.Project	true		"body for Project content"
// @Success 201 {int} models.Project
// @Failure 403 body is empty
// @router /create [post]
func (c *ProjectController) Create() {
	var v models.ProjectDetail
	json.Unmarshal(c.Ctx.Input.RequestBody, &v)
	secret := beego.AppConfig.String("developerJWTSecret")
	token, err := c.ParseToken(secret, helpers.DEVELOPER_TOKEN)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.ProjectResponse{
			Status:  "error",
			Code:    400,
			Message: err.Error(),
			Project: nil,
		}
		c.ServeJSON()
		return
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	developerId, _ := strconv.ParseInt(claims["id"].(string), 10, 64)
	if !ok {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.ProjectResponse{
			Status:  "error",
			Code:    400,
			Message: "Token permission error",
			Project: nil,
		}
		c.ServeJSON()
		return
	}
	v.DeveloperId = developerId
	project, err := models.AddProject(&v)
	if err == nil {
		c.Ctx.Output.SetStatus(201)
		c.Data["json"] = &models.ProjectResponse{
			Status:  "success",
			Code:    201,
			Message: "Project created successfully",
			Project: project,
		}
	} else {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.ProjectResponse{
			Status:  "error",
			Code:    500,
			Message: err.Error(),
			Project: nil,
		}
	}
	c.ServeJSON()
}

// VerifyProjectToken ...
// @Title Get One
// @Description get Project by id
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Project
// @Failure 403 :id is empty
// @router /verify_token [post]
func (c *ProjectController) VerifyProjectToken() {
	projectTokenBody := models.ProjectTokenBody{}
	err := json.Unmarshal(c.Ctx.Input.RequestBody, &projectTokenBody)
	//Verify API request from developer
	secret := beego.AppConfig.String("developerJWTSecret")
	token, err := c.ParseToken(secret, helpers.DEVELOPER_TOKEN)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.ProjectResponse{
			Status:  "error",
			Code:    400,
			Message: err.Error(),
			Project: nil,
		}
		c.ServeJSON()
		return
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	developerId, _ := claims["id"].(int)
	if !ok {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.ProjectResponse{
			Status:  "error",
			Code:    400,
			Message: "Token permission error",
			Project: nil,
		}
		c.ServeJSON()
		return
	}
	project, err := models.VerifyProject(projectTokenBody.Token, developerId)
	if err != nil {
		c.Ctx.Output.SetStatus(400)
		c.Data["json"] = &models.ProjectResponse{
			Status:  "error",
			Code:    400,
			Message: err.Error(),
			Project: nil,
		}
		c.ServeJSON()
		return
	}
	c.Ctx.Output.SetStatus(200)
	c.Data["json"] = &models.ProjectResponse{
		Status:  "success",
		Code:    200,
		Message: "project successfully verified",
		Project: project,
	}
	c.ServeJSON()
}

// GetNanoappsOfProject
// @Title Get All Projects Of User
// @Description get Project by user
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Project
// @Failure 403 :id is empty
// @router /get_nanoapps [get]
func (c *ProjectController) GetNanoappsOfProject() {
	projectId := c.GetString("project_id")
	projectToken := c.GetString("project_token")
	project := &models.Project{}
	var err error
	if len(projectId) > 0 {
		id, _ := strconv.ParseInt(projectId, 0, 64)
		secret := beego.AppConfig.String("developerJWTSecret")
		_, err := c.ParseToken(secret, helpers.DEVELOPER_TOKEN)
		if err != nil {
			c.Ctx.Output.SetStatus(500)
			c.Data["json"] = &models.ProjectNanoappsResponse{
				Status:   "error",
				Code:     400,
				Message:  err.Error(),
				Nanoapps: nil,
			}
			beego.Error(err.Error())
			c.ServeJSON()
			return
		}
		project, err = models.GetProjectById(id)
	} else if len(projectToken) > 0 {
		project, err = models.GetProjectByToken(projectToken)
	}
	if project.Id == 0 {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.ProjectNanoappsResponse{
			Status:   "error",
			Code:     500,
			Message:  "Project token or id is invalid",
			Nanoapps: nil,
		}
		c.ServeJSON()
		return
	}

	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.ProjectNanoappsResponse{
			Status:   "error",
			Code:     400,
			Message:  err.Error(),
			Nanoapps: nil,
		}
		c.ServeJSON()
		return
	}
	nanoapps, err := models.GetNanoappsForProject(project.Id)
	c.Ctx.Output.SetStatus(200)
	c.Data["json"] = &models.ProjectNanoappsResponse{Status: "success",
		Code:     200,
		Message:  "nanoapps fetched successfully",
		Nanoapps: nanoapps,
	}
	c.ServeJSON()

}

// GetAllProjectsOfUser ...
// Consult
// @Title Get All Projects Of User
// @Description get Project by user
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Project
// @Failure 403 :id is empty
// @router /get_projects_of_dev [get]
func (c *ProjectController) GetProjectsOfDev() {
	secret := beego.AppConfig.String("developerJWTSecret")
	token, err := c.ParseToken(secret, helpers.DEVELOPER_TOKEN)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.ProjectResponse{
			Status:  "error",
			Code:    400,
			Message: err.Error(),
			Project: nil,
		}
		c.ServeJSON()
		return
	}
	claims, _ := token.Claims.(jwt.MapClaims)
	developerId, _ := strconv.ParseInt(claims["id"].(string), 10, 64)
	devProjects, err := models.GetAllDeveloperProjects(developerId)
	if err == nil {
		c.Ctx.Output.SetStatus(200)
		c.Data["json"] = &models.DeveloperProjectsResponse{Status: "success",
			Code:               200,
			Message:            "Developer projects fetched successfully",
			ProjectAndNanoapps: &devProjects,
		}
	} else {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DeveloperProjectsResponse{Status: "failure",
			Code:               500,
			Message:            "Error occured",
			ProjectAndNanoapps: nil,
		}
	}
	c.ServeJSON()
}

// Update the project ...
// Consult
// @Title Get All Projects Of User
// @Description get Project by user
// @Param	id		path 	string	true		"The key for staticblock"
// @Success 200 {object} models.Project
// @Failure 403 :id is empty
// @router /update [post]
func (c *ProjectController) UpdateProject() {
	secret := beego.AppConfig.String("developerJWTSecret")
	token, err := c.ParseToken(secret, helpers.DEVELOPER_TOKEN)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.ProjectResponse{
			Status:  "error",
			Code:    400,
			Message: err.Error(),
			Project: nil,
		}
		c.ServeJSON()
		return
	}
	_, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.ProjectResponse{
			Status:  "error",
			Code:    400,
			Message: "Token permission error",
			Project: nil,
		}
		c.ServeJSON()
		return
	}
	project := models.Project{}
	err = json.Unmarshal(c.Ctx.Input.RequestBody, &project)
	if err != nil {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Code:    500,
			Message: "Error occured: " + err.Error(),
		}
	}
	err = models.UpdateProject(&project)
	if err == nil {
		c.Ctx.Output.SetStatus(200)
		c.Data["json"] = &models.DefaultResponse{Status: "success",
			Code:    200,
			Message: "Project updated successfully",
		}
	} else {
		c.Ctx.Output.SetStatus(500)
		c.Data["json"] = &models.DefaultResponse{Status: "failure",
			Code:    500,
			Message: "Error occured: " + err.Error(),
		}
	}
	c.ServeJSON()
}
