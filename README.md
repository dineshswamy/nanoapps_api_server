

#Command to create and run migration

bee migrate -driver=mysql -conn="root:@tcp(127.0.0.1:3306)/nanoapps_dev"
bee migrate rollback -driver=mysql -conn="root:@tcp(127.0.0.1:3306)/nanoapps_dev"
bee generate migration "create_models"
bee generate migration "create_models"
bee generate controller "device"
bee generate model modelname --fields="id:int"
lsof -i tcp:3000
netstat -vanp tcp | grep 3000

#Convert string to int64
strconv.ParseInt(claims["id"].(string), 10, 64)

bee generate controller "abtest"
