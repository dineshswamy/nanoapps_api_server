package helpers

import (
	"crypto/md5"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"strings"

	fcm "github.com/NaySoftware/go-fcm"
	"github.com/astaxie/beego"
)

const USER_TOKEN = "X-Nanoapp-User-Token"
const DEVICE_TOKEN = "X-Nanoapp-Device-Token"
const DEVELOPER_TOKEN = "X-Nanoapp-Token"

func UploadFile(fileHeader *multipart.FileHeader, filePath string, fileName string) (string, error) {
	file, err := fileHeader.Open()
	defer file.Close()
	if err != nil {
		beego.Error("Error opening the file", err)
		return "", errors.New("Error occured while opening the file")
	}
	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		beego.Error("Copy File to Hash:", err)
		return "", errors.New("Error while copying file to hash")
	}
	hex := fmt.Sprintf("%x", hash.Sum(nil))

	//Checking whether file/folder exists
	_, err = os.Stat(filePath)
	if os.IsNotExist(err) {
		//appending a slash to create directory
		err = os.MkdirAll(filePath, 0700)
	}
	filePathName := strings.Join([]string{filePath, fileName}, "/")
	//Will be moved to a CDN
	dst, err := os.Create(filePathName)
	if err != nil {
		beego.Error("Create File:", err)
		return "", errors.New("Error while creating files")
	}
	defer dst.Close()
	file.Seek(0, 0)
	if _, err := io.Copy(dst, file); err != nil {
		beego.Error("Copy File:", err)
		return "", errors.New("Error while copying files")
	}
	beego.Info("Completed - Processing files")
	return hex, nil
}

func SendPushNotification(serverApiKey string, tokens []string, data map[string]string) {
	c := fcm.NewFcmClient(serverApiKey)
	c.NewFcmRegIdsMsg(tokens, data)
	status, err := c.Send()
	if err == nil {
		status.PrintResults()
	} else {
		fmt.Println(err)
	}
}
